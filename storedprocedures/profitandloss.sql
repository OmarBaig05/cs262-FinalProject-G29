USE [ACDMS]
GO

/****** Object:  StoredProcedure [dbo].[GenerateProfitAndLossStatement]    Script Date: 5/4/2024 3:00:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GenerateProfitAndLossStatement]
    @StartDate DATE,
    @EndDate DATE
AS
BEGIN
    SELECT 
        'Salaries' AS Description,
        SUM(EmployeeSalary) AS Amount
    FROM 
        EmployeeSalary
    WHERE 
        DateOfPaid BETWEEN @StartDate AND @EndDate

    UNION ALL

    SELECT 
        'Expenses' AS Description,
        SUM(Amount) AS Amount
    FROM 
        Dues
    WHERE 
        LateSubmission = 1
        AND DueDate BETWEEN @StartDate AND @EndDate

    UNION ALL

    SELECT 
        'Revenue' AS Description,
        SUM(Amount) AS Amount
    FROM 
        Dues
    WHERE 
        LateSubmission = 0
        AND PaymentStatus = 1
        AND DueDate BETWEEN @StartDate AND @EndDate
END
GO


