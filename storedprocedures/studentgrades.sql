USE [ACDMS]
GO

/****** Object:  StoredProcedure [dbo].[GetStudentGrades]    Script Date: 5/4/2024 3:00:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetStudentGrades]
    @ResultID INT = NULL
AS
BEGIN
    -- Create or truncate temporary table
    IF OBJECT_ID('tempdb..#StudentGrades') IS NOT NULL
        DROP TABLE #StudentGrades;

    CREATE TABLE #StudentGrades (
        StudentID INT,
        TotalObtainedMarks DECIMAL(10, 2),
        TotalMarks DECIMAL(10, 2),
        Percentage DECIMAL(10, 2),
        Grade NVARCHAR(10)
    );

    -- Insert new data into #StudentGrades
    IF @ResultID IS NULL OR @ResultID = '*'
    BEGIN
        INSERT INTO #StudentGrades (StudentID, TotalObtainedMarks, TotalMarks, Percentage, Grade)
        SELECT 
            R.StudentID,
            SUM(R.ObtainedMarks) AS TotalObtainedMarks,
            SUM(R.TotalMarks) AS TotalMarks,
            CONVERT(DECIMAL(10, 2), (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0))) AS Percentage,
            CASE 
                WHEN (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0)) >= 90 THEN 'A+'
                WHEN (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0)) >= 80 THEN 'A'
                WHEN (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0)) >= 70 THEN 'B'
                WHEN (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0)) >= 60 THEN 'C'
                ELSE 'F'
            END AS Grade
        FROM Results R
        GROUP BY R.StudentID;
    END
    ELSE
    BEGIN
        INSERT INTO #StudentGrades (StudentID, TotalObtainedMarks, TotalMarks, Percentage, Grade)
        SELECT 
            R.StudentID,
            SUM(R.ObtainedMarks) AS TotalObtainedMarks,
            SUM(R.TotalMarks) AS TotalMarks,
            CONVERT(DECIMAL(10, 2), (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0))) AS Percentage,
            CASE 
                WHEN (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0)) >= 90 THEN 'A+'
                WHEN (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0)) >= 80 THEN 'A'
                WHEN (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0)) >= 70 THEN 'B'
                WHEN (SUM(R.ObtainedMarks) * 100.0 / NULLIF(SUM(R.TotalMarks), 0)) >= 60 THEN 'C'
                ELSE 'F'
            END AS Grade
        FROM Results R
        WHERE R.ResultID = @ResultID
        GROUP BY R.StudentID;
    END

    -- Select data from temporary table
    SELECT * FROM #StudentGrades;
END
GO


