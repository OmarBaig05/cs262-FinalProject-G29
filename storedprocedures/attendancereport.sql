


USE [ACDMS]
GO

/****** Object:  StoredProcedure [dbo].[GenerateClassWiseAttendanceReport]    Script Date: 5/4/2024 2:59:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GenerateClassWiseAttendanceReport]
    @StartDate DATE,
    @EndDate DATE
AS
BEGIN
    SELECT 
        Class.Name AS ClassName,
        Person.Name AS StudentName,
        Attendance.AttendanceDate,
        COUNT(Attendance.AttendanceStatus) AS TotalDaysPresent
    FROM 
        Class
        JOIN Subject ON Class.ClassID = Subject.ClassID
        JOIN Enrollments ON Subject.SubjectID = Enrollments.SubjectID
        JOIN Person ON Enrollments.PersonID = Person.ID
        LEFT JOIN Attendance ON Enrollments.PersonID = Attendance.PersonID
                              AND Attendance.AttendanceDate BETWEEN @StartDate AND @EndDate
                              AND Attendance.AttendanceStatus = 1
    GROUP BY 
        Class.Name, Person.Name, Attendance.AttendanceDate
END
GO


