CREATE VIEW TeacherInformationView AS
SELECT
    P.ID,
    P.Name AS TeacherName,
    P.Email AS TeacherEmail,
    P.ContactNumber AS TeacherContactNumber,
    P.CNIC AS TeacherCNIC,
    P.Gender AS TeacherGender,
    P.JoiningDate AS TeacherJoiningDate,
	SB.Name AS SubjectName
    
FROM
    Employee E
    INNER JOIN Person P ON E.ID = P.ID
	INNER JOIN Subject SB ON E.ID = SB.SubjectID
	INNER JOIN Enrollments ER ON ER.SubjectID = SB.SubjectID
    

Where E.Role = 'Teacher'



;