CREATE VIEW StudentInformationView AS
SELECT
    S.StudentID,
    P.Name AS StudentName,
    P.Email AS StudentEmail,
    P.ContactNumber AS StudentContactNumber,
    P.CNIC AS StudentCNIC,
    P.Gender AS StudentGender,
    P.JoiningDate AS StudentJoiningDate
    
FROM
    Student S
    INNER JOIN Person P ON S.ID = P.ID
    INNER JOIN Enrollments E ON S.ID = E.PersonID
    INNER JOIN Subject SB ON E.SubjectID = SB.SubjectID;
