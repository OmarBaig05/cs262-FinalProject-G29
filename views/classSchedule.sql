-- Class Schedule View
CREATE VIEW ClassScheduleView AS
SELECT
    E.ID,
    C.Name AS ClassName,
	SB.Name AS SubjectName,
	SB.Status AS SubjectStatus
    
FROM
    Subject SB
	INNER JOIN Class C ON C.ClassID = SB.ClassID
	INNER JOIN Enrollments ER ON ER.SubjectID = ER.SubjectID
    INNER JOIN Person P ON P.ID = ER.PersonID
	INNER JOIN Employee E ON E.ID = ER.PersonID
;
    