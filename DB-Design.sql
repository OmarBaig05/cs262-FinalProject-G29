-- Create Lookup table
CREATE TABLE Lookup (
    ID INT PRIMARY KEY,
    Value VARCHAR(50),
    Category VARCHAR(50)
);

-- Create Person table
CREATE TABLE Person (
    ID INT PRIMARY KEY,
    Name VARCHAR(100),
    FatherName VARCHAR(100),
    Email VARCHAR(100),
    ContactNumber VARCHAR(20),
    CNIC VARCHAR(15),
    Status INT FOREIGN KEY REFERENCES Lookup(ID),
    Gender INT FOREIGN KEY REFERENCES Lookup(ID),
    JoiningDate DATETIME
);

-- Create Student table
CREATE TABLE Student (
    StudentID VARCHAR(10) PRIMARY KEY,
    ID INT FOREIGN KEY REFERENCES Person(ID)
);

-- Create Employee table
CREATE TABLE Employee (
    EmployeeID VARCHAR(10) PRIMARY KEY,
    ID INT FOREIGN KEY REFERENCES Person(ID),
    Salary DECIMAL,
    Role INT REFERENCES Lookup(ID)
);

-- Create Authentication table
CREATE TABLE Authentication (
    ID INT PRIMARY KEY,
    Password VARCHAR(20),
    FOREIGN KEY (ID) REFERENCES Person(ID)
);
-- Create Class table
CREATE TABLE Class (
    ClassID INT PRIMARY KEY IDENTITY(1,1),
    Name VARCHAR(10),
    Discipline VARCHAR(30)
);
-- Create Admin table
CREATE TABLE Admin (
    AdminID VARCHAR(10) PRIMARY KEY,
    ID INT FOREIGN KEY REFERENCES Person(ID)
);

-- Create Subject table
CREATE TABLE Subject (
    SubjectID INT PRIMARY KEY IDENTITY(1,1),
    Name VARCHAR(50),
    ClassID INT,
    Status INT,
    FOREIGN KEY (ClassID) REFERENCES Class(ClassID),
    FOREIGN KEY (Status) REFERENCES Lookup(ID)
);




-- Create Enrollments table
CREATE TABLE Enrollments (
    EnrollmentDate DATETIME,
    PersonID INT FOREIGN KEY REFERENCES Person(ID),
    SubjectID INT FOREIGN KEY REFERENCES Subject(SubjectID),
    PRIMARY KEY (PersonID, SubjectID)
);

-- Create Results table
CREATE TABLE Results (
    ResultID INT PRIMARY KEY IDENTITY(1,1),
    StudentID VARCHAR(10) FOREIGN KEY REFERENCES Student(StudentID),
    TeacherID VARCHAR(10) FOREIGN KEY REFERENCES Employee(EmployeeID),
    SubjectID INT FOREIGN KEY REFERENCES Subject(SubjectID),
    ObtainedMarks DECIMAL,
    TotalMarks DECIMAL,
    ResultDate DATETIME
);

-- Create Dues table
CREATE TABLE Dues (
    DuesID INT PRIMARY KEY IDENTITY(1,1),
    StudentID VARCHAR(10) FOREIGN KEY REFERENCES Student(StudentID),
    Amount DECIMAL,
    DueDate DATE,
    LateSubmission INT FOREIGN KEY REFERENCES Lookup(ID),
    PaymentStatus INT FOREIGN KEY REFERENCES Lookup(ID)
);

-- Create EmployeeSalary table
CREATE TABLE EmployeeSalary (
    EmployeeID VARCHAR(10) FOREIGN KEY REFERENCES Employee(EmployeeID),
    EmployeeSalary DECIMAL,
    Status INT FOREIGN KEY REFERENCES Lookup(ID),
    DateOfPaid DATE
);

-- Create Challan table
CREATE TABLE Challan (
    ChallanID INT PRIMARY KEY IDENTITY(1,1),
    DuesID INT FOREIGN KEY REFERENCES Dues(DuesID),
    ChallanNumber VARCHAR(20),
    DatePaid DATE
);

-- Create History table
CREATE TABLE History (
    RecordID INT PRIMARY KEY IDENTITY(1,1),
    ID INT FOREIGN KEY REFERENCES Person(ID),
    ChangedEntity VARCHAR(100),
    ChangeType VARCHAR(50),
    TupleID INT,
    ChangeTime TIMESTAMP
);
ALTER TABLE Person
ADD CONSTRAINT CHK_Person_PhoneNumberLength CHECK (LEN(ContactNumber) = 11);

ALTER TABLE Person
ADD CONSTRAINT CHK_Person_CNICLength CHECK (LEN(CNIC) = 15);