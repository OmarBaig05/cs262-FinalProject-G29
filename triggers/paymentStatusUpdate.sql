CREATE TRIGGER UpdatePaymentStatus
ON Challan
AFTER INSERT
AS
BEGIN
    UPDATE D
    SET PaymentStatus = 1
    FROM Dues D
    INNER JOIN inserted I ON D.DuesID = I.DuesID
    WHERE D.PaymentStatus = 1; -- Assuming 1 means paid
END;
