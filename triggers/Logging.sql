CREATE TRIGGER LogAdminChanges
ON Admin
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    DECLARE @Action NVARCHAR(20);

    IF EXISTS(SELECT * FROM inserted) AND EXISTS(SELECT * FROM deleted)
        SET @Action = 'Update';
    ELSE IF EXISTS(SELECT * FROM inserted)
        SET @Action = 'Insert';
    ELSE
        SET @Action = 'Delete';

    INSERT INTO AdminHistory (AdminID, ChangedEntity, ChangeType)
    SELECT 
        CASE WHEN @Action = 'Insert' THEN ins.AdminID ELSE del.AdminID END,
        'Admin',
        @Action
    FROM inserted ins
    FULL OUTER JOIN deleted del ON ins.AdminID = del.AdminID;
END;
