CREATE TRIGGER PreventExceedingMarks
ON Results
AFTER INSERT
AS
BEGIN
    IF EXISTS (SELECT 1 FROM inserted WHERE ObtainedMarks > TotalMarks)
    BEGIN
        RAISERROR ('Obtained Marks cannot exceed Total Marks.', 16, 1)
        ROLLBACK TRANSACTION
    END
END;
