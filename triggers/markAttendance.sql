CREATE TRIGGER MarkEmployeeAttendance
ON Authentication
AFTER INSERT
AS
BEGIN
    DECLARE @CurrentTime TIME = GETDATE();
    DECLARE @WorkingHoursStart TIME = '08:00:00'; -- Assuming working hours start at 8 AM
    DECLARE @WorkingHoursEnd TIME = '17:00:00'; -- Assuming working hours end at 5 PM

    IF @CurrentTime BETWEEN @WorkingHoursStart AND @WorkingHoursEnd
    BEGIN
        UPDATE Attendance
        SET AttendanceStatus = 1 -- Assuming 1 means present
        WHERE PersonID IN (SELECT PersonID FROM inserted);
    END;
END;
