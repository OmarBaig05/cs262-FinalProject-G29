CREATE TRIGGER MaintainReferentialIntegrity
ON Person
AFTER DELETE
AS
BEGIN
    DELETE FROM Student WHERE ID IN (SELECT ID FROM deleted);
    DELETE FROM Employee WHERE ID IN (SELECT ID FROM deleted);
    DELETE FROM Authentication WHERE ID IN (SELECT ID FROM deleted);
    -- Add more related tables here
END;
