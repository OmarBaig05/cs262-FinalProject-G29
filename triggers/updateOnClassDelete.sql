CREATE TRIGGER MaintainDataConsistencyOnClassDeletion
ON Class
AFTER DELETE
AS
BEGIN

    DELETE FROM Subject WHERE ClassID IN (SELECT ClassID FROM deleted);
    
END;
