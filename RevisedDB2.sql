USE [master]
GO
/****** Object:  Database [ACDMS]    Script Date: 5/10/2024 6:52:56 PM ******/
CREATE DATABASE [ACDMS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ACDMS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\ACDMS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ACDMS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\ACDMS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [ACDMS] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ACDMS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ACDMS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ACDMS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ACDMS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ACDMS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ACDMS] SET ARITHABORT OFF 
GO
ALTER DATABASE [ACDMS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ACDMS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ACDMS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ACDMS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ACDMS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ACDMS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ACDMS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ACDMS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ACDMS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ACDMS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ACDMS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ACDMS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ACDMS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ACDMS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ACDMS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ACDMS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ACDMS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ACDMS] SET RECOVERY FULL 
GO
ALTER DATABASE [ACDMS] SET  MULTI_USER 
GO
ALTER DATABASE [ACDMS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ACDMS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ACDMS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ACDMS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ACDMS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ACDMS] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'ACDMS', N'ON'
GO
ALTER DATABASE [ACDMS] SET QUERY_STORE = ON
GO
ALTER DATABASE [ACDMS] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [ACDMS]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminID] [varchar](10) NOT NULL,
	[ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attendance]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attendance](
	[AttendanceID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[AttendanceDate] [date] NULL,
	[AttendanceStatus] [int] NULL,
	[AttendanceType] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[AttendanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Authentication]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authentication](
	[ID] [int] NOT NULL,
	[Password] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Challan]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Challan](
	[ChallanID] [int] IDENTITY(1,1) NOT NULL,
	[DuesID] [int] NULL,
	[ChallanNumber] [varchar](20) NULL,
	[DatePaid] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[ChallanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[ClassID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](10) NULL,
	[Discipline] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dues]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dues](
	[DuesID] [int] IDENTITY(1,1) NOT NULL,
	[StudentID] [varchar](10) NULL,
	[Amount] [decimal](18, 0) NULL,
	[DueDate] [date] NULL,
	[LateSubmission] [int] NULL,
	[PaymentStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DuesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeID] [varchar](10) NOT NULL,
	[ID] [int] NULL,
	[Salary] [decimal](18, 0) NULL,
	[Role] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeSalary]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeSalary](
	[EmployeeID] [varchar](10) NULL,
	[EmployeeSalary] [decimal](18, 0) NULL,
	[Status] [int] NULL,
	[DateOfPaid] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Enrollments]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enrollments](
	[EnrollmentDate] [datetime] NULL,
	[PersonID] [int] NOT NULL,
	[SubjectID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[SubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[History]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History](
	[RecordID] [int] IDENTITY(1,1) NOT NULL,
	[ID] [int] NULL,
	[ChangedEntity] [varchar](100) NULL,
	[ChangeType] [varchar](50) NULL,
	[TupleID] [int] NULL,
	[ChangeTime] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookup]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup](
	[ID] [int] NOT NULL,
	[Value] [varchar](50) NULL,
	[Category] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[FatherName] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[ContactNumber] [varchar](20) NULL,
	[CNIC] [varchar](15) NULL,
	[Status] [int] NULL,
	[Gender] [int] NULL,
	[JoiningDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Results]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Results](
	[ResultID] [int] IDENTITY(1,1) NOT NULL,
	[StudentID] [varchar](10) NULL,
	[TeacherID] [varchar](10) NULL,
	[SubjectID] [int] NULL,
	[ObtainedMarks] [decimal](18, 0) NULL,
	[TotalMarks] [decimal](18, 0) NULL,
	[ResultDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ResultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[StudentID] [varchar](10) NOT NULL,
	[ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subject]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject](
	[SubjectID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[ClassID] [int] NULL,
	[Status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Admin]  WITH CHECK ADD FOREIGN KEY([ID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_AttendanceStatus] FOREIGN KEY([AttendanceStatus])
REFERENCES [dbo].[Lookup] ([ID])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_AttendanceStatus]
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_Person]
GO
ALTER TABLE [dbo].[Authentication]  WITH CHECK ADD FOREIGN KEY([ID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Challan]  WITH CHECK ADD FOREIGN KEY([DuesID])
REFERENCES [dbo].[Dues] ([DuesID])
GO
ALTER TABLE [dbo].[Dues]  WITH CHECK ADD FOREIGN KEY([LateSubmission])
REFERENCES [dbo].[Lookup] ([ID])
GO
ALTER TABLE [dbo].[Dues]  WITH CHECK ADD FOREIGN KEY([PaymentStatus])
REFERENCES [dbo].[Lookup] ([ID])
GO
ALTER TABLE [dbo].[Dues]  WITH CHECK ADD FOREIGN KEY([StudentID])
REFERENCES [dbo].[Student] ([StudentID])
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD FOREIGN KEY([ID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD FOREIGN KEY([Role])
REFERENCES [dbo].[Lookup] ([ID])
GO
ALTER TABLE [dbo].[EmployeeSalary]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[EmployeeSalary]  WITH CHECK ADD FOREIGN KEY([Status])
REFERENCES [dbo].[Lookup] ([ID])
GO
ALTER TABLE [dbo].[Enrollments]  WITH CHECK ADD FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Enrollments]  WITH CHECK ADD FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([SubjectID])
GO
ALTER TABLE [dbo].[History]  WITH CHECK ADD FOREIGN KEY([ID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD FOREIGN KEY([Gender])
REFERENCES [dbo].[Lookup] ([ID])
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD FOREIGN KEY([Status])
REFERENCES [dbo].[Lookup] ([ID])
GO
ALTER TABLE [dbo].[Results]  WITH CHECK ADD FOREIGN KEY([StudentID])
REFERENCES [dbo].[Student] ([StudentID])
GO
ALTER TABLE [dbo].[Results]  WITH CHECK ADD FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([SubjectID])
GO
ALTER TABLE [dbo].[Results]  WITH CHECK ADD FOREIGN KEY([TeacherID])
REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD FOREIGN KEY([ID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD FOREIGN KEY([ClassID])
REFERENCES [dbo].[Class] ([ClassID])
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD FOREIGN KEY([Status])
REFERENCES [dbo].[Lookup] ([ID])
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [CHK_Person_CNICLength] CHECK  ((len([CNIC])=(15)))
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [CHK_Person_CNICLength]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [CHK_Person_PhoneNumberLength] CHECK  ((len([ContactNumber])=(11)))
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [CHK_Person_PhoneNumberLength]
GO
/****** Object:  StoredProcedure [dbo].[GenerateClassWiseAttendanceReport]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GenerateClassWiseAttendanceReport]
    @StartDate DATE,
    @EndDate DATE
AS
BEGIN
    SELECT 
        Class.Name AS ClassName,
        Person.Name AS StudentName,
        Attendance.AttendanceDate,
        COUNT(Attendance.AttendanceStatus) AS TotalDaysPresent
    FROM 
        Class
        JOIN Subject ON Class.ClassID = Subject.ClassID
        JOIN Enrollments ON Subject.SubjectID = Enrollments.SubjectID
        JOIN Person ON Enrollments.PersonID = Person.ID
        LEFT JOIN Attendance ON Enrollments.PersonID = Attendance.PersonID
                              AND Attendance.AttendanceDate BETWEEN @StartDate AND @EndDate
                              AND Attendance.AttendanceStatus = 1
    GROUP BY 
        Class.Name, Person.Name, Attendance.AttendanceDate
END
GO
/****** Object:  StoredProcedure [dbo].[GetTodaysAttendanceReport]    Script Date: 5/10/2024 6:52:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTodaysAttendanceReport]
AS
BEGIN
  -- Get today's date
  DECLARE @todayDate DATE = GETDATE();

  SELECT *
  FROM
    Attendance
    where Attendance.AttendanceDate = @todayDate
  
;
END
GO
USE [master]
GO
ALTER DATABASE [ACDMS] SET  READ_WRITE 
GO
