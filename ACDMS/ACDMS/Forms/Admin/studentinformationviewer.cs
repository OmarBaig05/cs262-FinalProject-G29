﻿using ACDMS.Supporting_Classes;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Admin
{
    public partial class studentinformationviewer : Form
    {
        private ReportDocument reportDocument;

        private Configuration configuration;
        public studentinformationviewer()
        {
            InitializeComponent();
            configuration = Configuration.getInstance();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void LoadReport()
        {
            try
            {
                reportDocument = new ReportDocument();
                string reportPath = @"E:\University\4th SEMESTER\DB Lab\Final Term Project\cs262-FinalProject-G29\ACDMS\ACDMS\Forms\Admin\classSchedule.rpt";
                reportDocument.Load(reportPath);

                // Set report source
                reportDocument.SetDataSource(GetDataFromView("StudentInformationView"));

                // Set report document to report viewer
                crystalReportViewer1.ReportSource = reportDocument;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private DataTable GetDataFromView(string viewName)
        {
            DataTable dataTable = new DataTable();
            try
            {
                string query = "SELECT * FROM " + viewName;
                SqlCommand command = new SqlCommand(query, configuration.getConnection());
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dataTable;
        }
    }
}
