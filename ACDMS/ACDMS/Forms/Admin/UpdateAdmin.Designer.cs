﻿namespace ACDMS.Forms.Admin
{
    partial class UpdateAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Update_btn = new System.Windows.Forms.Button();
            this.Password_textBox = new System.Windows.Forms.TextBox();
            this.Name_textBox = new System.Windows.Forms.TextBox();
            this.Gender_CB = new System.Windows.Forms.ComboBox();
            this.Email_textBox = new System.Windows.Forms.TextBox();
            this.CNIC_textBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Status_CB = new System.Windows.Forms.ComboBox();
            this.ID_textBox = new System.Windows.Forms.TextBox();
            this.FatherName_textBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(317, 49);
            this.label1.TabIndex = 16;
            this.label1.Text = "Name";
            // 
            // Update_btn
            // 
            this.Update_btn.Location = new System.Drawing.Point(326, 395);
            this.Update_btn.Name = "Update_btn";
            this.Update_btn.Size = new System.Drawing.Size(368, 47);
            this.Update_btn.TabIndex = 9;
            this.Update_btn.Text = "Update";
            this.Update_btn.UseVisualStyleBackColor = true;
            this.Update_btn.Click += new System.EventHandler(this.Update_btn_Click);
            // 
            // Password_textBox
            // 
            this.Password_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Password_textBox.Location = new System.Drawing.Point(326, 101);
            this.Password_textBox.Name = "Password_textBox";
            this.Password_textBox.Size = new System.Drawing.Size(368, 26);
            this.Password_textBox.TabIndex = 14;
            this.Password_textBox.Text = "Password";
            this.Password_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Name_textBox
            // 
            this.Name_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Name_textBox.Location = new System.Drawing.Point(326, 3);
            this.Name_textBox.Name = "Name_textBox";
            this.Name_textBox.Size = new System.Drawing.Size(368, 26);
            this.Name_textBox.TabIndex = 0;
            this.Name_textBox.Text = "Name";
            this.Name_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Gender_CB
            // 
            this.Gender_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Gender_CB.FormattingEnabled = true;
            this.Gender_CB.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Prefer Not To Say"});
            this.Gender_CB.Location = new System.Drawing.Point(326, 199);
            this.Gender_CB.Name = "Gender_CB";
            this.Gender_CB.Size = new System.Drawing.Size(368, 28);
            this.Gender_CB.TabIndex = 10;
            // 
            // Email_textBox
            // 
            this.Email_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Email_textBox.Location = new System.Drawing.Point(326, 150);
            this.Email_textBox.Name = "Email_textBox";
            this.Email_textBox.Size = new System.Drawing.Size(368, 26);
            this.Email_textBox.TabIndex = 13;
            this.Email_textBox.Text = "Email";
            this.Email_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CNIC_textBox
            // 
            this.CNIC_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CNIC_textBox.Location = new System.Drawing.Point(326, 52);
            this.CNIC_textBox.Name = "CNIC_textBox";
            this.CNIC_textBox.Size = new System.Drawing.Size(368, 26);
            this.CNIC_textBox.TabIndex = 11;
            this.CNIC_textBox.Text = "CNIC";
            this.CNIC_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(317, 49);
            this.label2.TabIndex = 17;
            this.label2.Text = "CNIC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(317, 49);
            this.label3.TabIndex = 18;
            this.label3.Text = "Password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(317, 49);
            this.label4.TabIndex = 19;
            this.label4.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(317, 49);
            this.label5.TabIndex = 20;
            this.label5.Text = "Gender";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 245);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(317, 49);
            this.label6.TabIndex = 21;
            this.label6.Text = "Status";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 374F));
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.CNIC_textBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Email_textBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.Gender_CB, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.Name_textBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Password_textBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Status_CB, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.Update_btn, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.ID_textBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.FatherName_textBox, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 7);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(56, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11069F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11069F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11069F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11069F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11069F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11069F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11069F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11403F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(697, 445);
            this.tableLayoutPanel1.TabIndex = 4;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // Status_CB
            // 
            this.Status_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Status_CB.FormattingEnabled = true;
            this.Status_CB.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.Status_CB.Location = new System.Drawing.Point(326, 248);
            this.Status_CB.Name = "Status_CB";
            this.Status_CB.Size = new System.Drawing.Size(368, 28);
            this.Status_CB.TabIndex = 22;
            // 
            // ID_textBox
            // 
            this.ID_textBox.Location = new System.Drawing.Point(326, 297);
            this.ID_textBox.Name = "ID_textBox";
            this.ID_textBox.Size = new System.Drawing.Size(368, 26);
            this.ID_textBox.TabIndex = 23;
            this.ID_textBox.Text = "ID";
            this.ID_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FatherName_textBox
            // 
            this.FatherName_textBox.Location = new System.Drawing.Point(326, 346);
            this.FatherName_textBox.Name = "FatherName_textBox";
            this.FatherName_textBox.Size = new System.Drawing.Size(368, 26);
            this.FatherName_textBox.TabIndex = 24;
            this.FatherName_textBox.Text = "FatherName";
            this.FatherName_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 20);
            this.label7.TabIndex = 25;
            this.label7.Text = "ID";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 343);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 20);
            this.label9.TabIndex = 26;
            this.label9.Text = "FatherName";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label8.Location = new System.Drawing.Point(269, -54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(294, 46);
            this.label8.TabIndex = 5;
            this.label8.Text = "SIGN UP Menu";
            // 
            // UpdateAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UpdateAdmin";
            this.Text = "UpdateAdmin";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Update_btn;
        private System.Windows.Forms.TextBox Password_textBox;
        private System.Windows.Forms.TextBox Name_textBox;
        private System.Windows.Forms.ComboBox Gender_CB;
        private System.Windows.Forms.TextBox Email_textBox;
        private System.Windows.Forms.TextBox CNIC_textBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox Status_CB;
        private System.Windows.Forms.TextBox ID_textBox;
        private System.Windows.Forms.TextBox FatherName_textBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
    }
}