﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Admin
{
    public partial class UpdateAdmin : Form
    {
        public UpdateAdmin()
        {
            InitializeComponent();
        }

        private void Update_btn_Click(object sender, EventArgs e)
        {
            string id = ID_textBox.Text.Trim();
            string email = Email_textBox.Text.Trim();
            string name = Name_textBox.Text.Trim();
            string pass = Password_textBox.Text.Trim();
            string CNIC = CNIC_textBox.Text.Trim();
            string contact = CNIC_textBox.Text.Trim();
            string father = FatherName_textBox.Text.Trim();
            string Gender = Gender_CB.SelectedItem?.ToString();
            string Status = Status_CB.SelectedItem?.ToString();
            int g = Functions.GetLookUpValue(Gender);
            int s = Functions.GetLookUpValue(Status);

            AdminBL a = new AdminBL(id, name, email, g, contact, CNIC, father, pass, s, DateTime.Now);
            int i = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);

            if (AdminDL.UpdateAdminData(a,i))
            {
                MessageBox.Show(id + " Added!");
                toggelPage();

            }

            toggelPage();
        }

        private void toggelPage()
        {
            AdminManagement a = new AdminManagement();
            this.Hide();
            a.Show();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
