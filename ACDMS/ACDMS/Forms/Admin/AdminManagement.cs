﻿using ACDMS.DL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Admin
{
    public partial class AdminManagement : Form
    {
        public AdminManagement()
        {
            InitializeComponent();
            DisplayDataOnDataGridView();
        }

        private void Exit_btn_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            this.Hide();
            a.ShowDialog();
        }

        private void Update_btn_Click(object sender, EventArgs e)
        {
            UpdateAdmin a = new UpdateAdmin();
            this.Hide();
            a.ShowDialog();
        }

        private void Delete_btn_Click(object sender, EventArgs e)
        {
            string Id = ID_textBox.Text.Trim();

            int i = Functions.GetPersonID(Id, "Admin");

            if(AdminDL.DeleteAdmin(Id, i))
            {
                MessageBox.Show("Admin Deleted");
                DisplayDataOnDataGridView();
            }
            else
            {
                MessageBox.Show("Their is some error");
            }
        }
        public void DisplayDataOnDataGridView()
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();
                string query = @"SELECT s.AdminID, p.Name, p.FatherName, p.ContactNumber, p.Email, p.CNIC, p.Gender AS Gender,p.Status AS Status, p.JoiningDate
                         FROM [dbo].[Person] p
                         INNER JOIN [dbo].[Admin] s ON p.ID = s.ID
                         INNER JOIN [dbo].[Authentication] l ON p.ID = l.ID";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
    }
}
