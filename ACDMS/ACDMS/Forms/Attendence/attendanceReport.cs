﻿using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Attendence
{
    public partial class attendanceReport : Form
    {

        public attendanceReport()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                DateTime startDate = dateTimePickerStartDate.Value.Date;
                DateTime endDate = dateTimePickerEndDate.Value.Date;


                ReportGenerator reportGenerator = new ReportGenerator();
                DataTable attendanceReport = reportGenerator.GenerateTodayAttendance();
                dataGridView.DataSource = attendanceReport;
                PowerBIAuthentication powerBIAuthentication = new PowerBIAuthentication();
                PowerBIManager p1 = new PowerBIManager();
                p1.PushAttendanceDailyTableToPowerBI(attendanceReport,"AttendanceTable");


            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}");
            }
        }

        private void GenerateReport_Click(object sender, EventArgs e)
        {
            try
            {

                DateTime startDate = dateTimePickerStartDate.Value.Date;
                DateTime endDate = dateTimePickerEndDate.Value.Date;


                ReportGenerator reportGenerator = new ReportGenerator();
                DataTable attendanceReport = reportGenerator.GenerateTodayAttendance();
                dataGridView.DataSource = attendanceReport;


            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}");
            }
        }

        private void attendanceReport_Load(object sender, EventArgs e)
        {

        }
    }
}
