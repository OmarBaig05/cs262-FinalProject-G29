﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Forms.Admin;
using ACDMS.Forms.Employee;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Attendence
{
    public partial class Manage_Attendance : Form
    {
        public Manage_Attendance()
        {
            InitializeComponent();
            DisplayDataOnDataGridView();
        }

        private void Insert_btn_Click(object sender, EventArgs e)
        {
            string id = ID_textBox.Text;
            string status = Status_textBox.Text;
            string role = Role_CB.SelectedItem?.ToString();
            DateTime date = dateTimePicker1.Value.Date;
            int d = Functions.GetPersonID(id, role);

            if (status == "A" || status == "a")
            {
                status = "Unactive";
            }
            else if (status == "P" || status == "p")
            {
                status = "Active";
            }
            else if (status == "L" || status == "l")
            {
                status = "Leave";
            }

            int s = Functions.GetLookUpValue(status);
            AttendanceBL a = new AttendanceBL(d, role, date, s);


            if(AttendanceDL.MarkAttendance(a))
            {
                MessageBox.Show("Attandence marked");
                DisplayDataOnDataGridView();
            }
            else
            {
                MessageBox.Show("Their is some error");
            }
        }

        private void attendancereportbutton_Click(object sender, EventArgs e)
        {
            attendanceReport a = new attendanceReport();
            this.Hide();
            a.ShowDialog();
        }

        private void Exit_btn_Click(object sender, EventArgs e)
        {
            string b = GlobalVar.LoggerID;
            if (b.Contains("EMP") || b.Contains("emp"))
            {
                Employee_Dashboard a = new Employee_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
            else
            {
                Admin_Dashboard a = new Admin_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
        }

        private void Delete_btn_Click(object sender, EventArgs e)
        {
            int id = int.Parse(textBox1.Text.Trim());
            int i = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);


            if (AttendanceDL.DeleteAttendance(id,i))
            {
                MessageBox.Show("Attandence Deleted");
                DisplayDataOnDataGridView();
            }
            else
            {
                MessageBox.Show("Their is some error");
            }
        }

        public void DisplayDataOnDataGridView()
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();
                string query = @"SELECT AttendanceID,PersonID,AttendanceDate,AttendanceStatus,AttendanceType FROM [dbo].[Attendance];
";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
    }
}
