﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Forms.Admin;
using ACDMS.Forms.Employee;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Results
{
    public partial class Results_Manage : Form
    {
        public Results_Manage()
        {
            InitializeComponent();
            DisplayDataOnDataGridView();
        }

        private void Delete_btn_Click(object sender, EventArgs e)
        {
            // Get the resultID from the the getResultID function in Functions.cs and pass it for updation and deletion
            try
            {
                string stdID = StudentID_textBox.Text.Trim();
                string TchID = TeacherID_textBox.Text.Trim();
                int subjectID = int.Parse(SubjectID_textBox.Text.Trim().ToString());

                int resultID = Functions.GetResultIDByStudentIDAndSubjectID(stdID,subjectID, TchID);
                int i = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);


                if (ResultsDL.DeleteResult(resultID,i))
                {
                    MessageBox.Show("Result Deleted");
                    DisplayDataOnDataGridView();
                }
            }
            catch
            {
                MessageBox.Show("Their occured an error, type the valid credentials");
            }

        }

        private void Update_btn_Click(object sender, EventArgs e)
        {
            UpdateResult updateResult = new UpdateResult();
            this.Hide();
            updateResult.ShowDialog();
        }

        private void Exit_btn_Click(object sender, EventArgs e)
        {
            string b = GlobalVar.LoggerID;
            if (b.Contains("EMP") || b.Contains("emp"))
            {
                Employee_Dashboard a = new Employee_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
            else
            {
                Admin_Dashboard a = new Admin_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
        }

        private void Insert_btn_Click(object sender, EventArgs e)
        {
            try
            {
                string stdID = StudentID_textBox.Text.Trim();
                string TchID = TeacherID_textBox.Text.Trim();
                int subjectID = int.Parse(SubjectID_textBox.Text.Trim().ToString());
                decimal totalMarks = decimal.Parse(TotalMarks_textBox.Text.Trim().ToString());
                decimal obtainedMarks = decimal.Parse(ObtainedMarks_textBox.Text.Trim().ToString());
                DateTime d = dateTimePicker.Value;

                int sID = Functions.GetPersonID(stdID, "Student");
                int TID = Functions.GetPersonID(TchID, "Employee");

                ResultsBL a = new ResultsBL(stdID, TchID, subjectID, obtainedMarks, totalMarks, d);

                if (ResultsDL.InsertResult(a))
                {
                    MessageBox.Show("Results Inserted");
                    DisplayDataOnDataGridView();
                }
            }
            catch {
                MessageBox.Show("Their occured an error, type the valid credentials");
            }
        }

        public void DisplayDataOnDataGridView()
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();
                string query = @"SELECT ResultID,StudentID,TeacherID,SubjectID,ObtainedMarks,TotalMarks,ResultDate FROM [dbo].[Results];
";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }


    }
}
