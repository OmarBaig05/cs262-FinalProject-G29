﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Results
{
    public partial class UpdateResult : Form
    {
        public UpdateResult()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Implement Update logic
            Results_Manage a = new Results_Manage();
            this.Hide(); a.ShowDialog();
        }
    }
}
