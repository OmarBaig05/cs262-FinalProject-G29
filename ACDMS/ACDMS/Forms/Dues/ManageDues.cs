﻿using ACDMS.BL;
using ACDMS.Forms.Admin;
using ACDMS.Forms.Employee;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Dues
{
    public partial class ManageDues : Form
    {
        public ManageDues()
        {
            InitializeComponent();
        }

        private void Insert_btn_Click(object sender, EventArgs e)
        {
            string studentID = studentID_textBox.Text.Trim();
            DateTime DueDate = dateTimePicker1.Value.Date;
            string Amount = Amount_textBox.Text.Trim();
            string paymentStatus = PaymentStatus_CB.SelectedItem?.ToString();
            string PaidOrNot = PaidOrNot_CB.SelectedItem?.ToString();



        }

        private void Exit_btn_Click(object sender, EventArgs e)
        {
            string b = GlobalVar.LoggerID;
            if (b.Contains("EMP") || b.Contains("emp"))
            {
                Employee_Dashboard a = new Employee_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
            else
            {
                Admin_Dashboard a = new Admin_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
        }

        private void Update_btn_Click(object sender, EventArgs e)
        {
            UpdateDues a = new UpdateDues();
            this.Hide(); a.ShowDialog();    
        }
    }
}
