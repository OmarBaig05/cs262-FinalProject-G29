﻿namespace ACDMS.Forms.Employee
{
    partial class Manage_Employee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.FatherName_textBox = new System.Windows.Forms.TextBox();
            this.Password_textbox = new System.Windows.Forms.TextBox();
            this.Status_CB = new System.Windows.Forms.ComboBox();
            this.Gender_CB = new System.Windows.Forms.ComboBox();
            this.CNIC_textBox = new System.Windows.Forms.TextBox();
            this.Role_CB = new System.Windows.Forms.ComboBox();
            this.Name_textBox = new System.Windows.Forms.TextBox();
            this.Salary_textBox = new System.Windows.Forms.TextBox();
            this.Contact_textBox = new System.Windows.Forms.TextBox();
            this.Email_textBox = new System.Windows.Forms.TextBox();
            this.ID_textBox = new System.Windows.Forms.TextBox();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Insert_btn = new System.Windows.Forms.Button();
            this.Delete_btn = new System.Windows.Forms.Button();
            this.Update_btn = new System.Windows.Forms.Button();
            this.Exit_btn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 40);
            this.label1.TabIndex = 13;
            this.label1.Text = "Deletion and Updation is on the basis of ID";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(200, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 732);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 189);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1028, 543);
            this.panel2.TabIndex = 20;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1028, 543);
            this.dataGridView1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.Controls.Add(this.FatherName_textBox, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.Password_textbox, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.Status_CB, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.Gender_CB, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.CNIC_textBox, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.Role_CB, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.Name_textBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Salary_textBox, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.Contact_textBox, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.Email_textBox, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.ID_textBox, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.dateTimePicker, 2, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1028, 189);
            this.tableLayoutPanel2.TabIndex = 19;
            // 
            // FatherName_textBox
            // 
            this.FatherName_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FatherName_textBox.Location = new System.Drawing.Point(3, 147);
            this.FatherName_textBox.Name = "FatherName_textBox";
            this.FatherName_textBox.Size = new System.Drawing.Size(336, 26);
            this.FatherName_textBox.TabIndex = 23;
            this.FatherName_textBox.Text = "Father Name";
            // 
            // Password_textbox
            // 
            this.Password_textbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Password_textbox.Location = new System.Drawing.Point(345, 147);
            this.Password_textbox.Name = "Password_textbox";
            this.Password_textbox.Size = new System.Drawing.Size(336, 26);
            this.Password_textbox.TabIndex = 22;
            this.Password_textbox.Text = "Password";
            // 
            // Status_CB
            // 
            this.Status_CB.AllowDrop = true;
            this.Status_CB.AutoCompleteCustomSource.AddRange(new string[] {
            "Male",
            "Female"});
            this.Status_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Status_CB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Status_CB.FormattingEnabled = true;
            this.Status_CB.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.Status_CB.Location = new System.Drawing.Point(687, 147);
            this.Status_CB.Name = "Status_CB";
            this.Status_CB.Size = new System.Drawing.Size(338, 28);
            this.Status_CB.TabIndex = 21;
            // 
            // Gender_CB
            // 
            this.Gender_CB.AllowDrop = true;
            this.Gender_CB.AutoCompleteCustomSource.AddRange(new string[] {
            "Male",
            "Female"});
            this.Gender_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Gender_CB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Gender_CB.FormattingEnabled = true;
            this.Gender_CB.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Prefer Not to Say"});
            this.Gender_CB.Location = new System.Drawing.Point(3, 99);
            this.Gender_CB.Name = "Gender_CB";
            this.Gender_CB.Size = new System.Drawing.Size(336, 28);
            this.Gender_CB.TabIndex = 20;
            // 
            // CNIC_textBox
            // 
            this.CNIC_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CNIC_textBox.Location = new System.Drawing.Point(345, 99);
            this.CNIC_textBox.Name = "CNIC_textBox";
            this.CNIC_textBox.Size = new System.Drawing.Size(336, 26);
            this.CNIC_textBox.TabIndex = 19;
            this.CNIC_textBox.Text = "CNIC";
            // 
            // Role_CB
            // 
            this.Role_CB.AllowDrop = true;
            this.Role_CB.AutoCompleteCustomSource.AddRange(new string[] {
            "Male",
            "Female"});
            this.Role_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Role_CB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Role_CB.FormattingEnabled = true;
            this.Role_CB.Items.AddRange(new object[] {
            "Employee",
            "Teacher"});
            this.Role_CB.Location = new System.Drawing.Point(687, 99);
            this.Role_CB.Name = "Role_CB";
            this.Role_CB.Size = new System.Drawing.Size(338, 28);
            this.Role_CB.TabIndex = 18;
            // 
            // Name_textBox
            // 
            this.Name_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Name_textBox.Location = new System.Drawing.Point(3, 3);
            this.Name_textBox.Name = "Name_textBox";
            this.Name_textBox.Size = new System.Drawing.Size(336, 26);
            this.Name_textBox.TabIndex = 11;
            this.Name_textBox.Text = "Name";
            // 
            // Salary_textBox
            // 
            this.Salary_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Salary_textBox.Location = new System.Drawing.Point(345, 3);
            this.Salary_textBox.Name = "Salary_textBox";
            this.Salary_textBox.Size = new System.Drawing.Size(336, 26);
            this.Salary_textBox.TabIndex = 12;
            this.Salary_textBox.Text = "Salary";
            // 
            // Contact_textBox
            // 
            this.Contact_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contact_textBox.Location = new System.Drawing.Point(687, 3);
            this.Contact_textBox.Name = "Contact_textBox";
            this.Contact_textBox.Size = new System.Drawing.Size(338, 26);
            this.Contact_textBox.TabIndex = 13;
            this.Contact_textBox.Text = "Contact Number";
            // 
            // Email_textBox
            // 
            this.Email_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Email_textBox.Location = new System.Drawing.Point(3, 51);
            this.Email_textBox.Name = "Email_textBox";
            this.Email_textBox.Size = new System.Drawing.Size(336, 26);
            this.Email_textBox.TabIndex = 14;
            this.Email_textBox.Text = "Email";
            // 
            // ID_textBox
            // 
            this.ID_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ID_textBox.Location = new System.Drawing.Point(345, 51);
            this.ID_textBox.Name = "ID_textBox";
            this.ID_textBox.Size = new System.Drawing.Size(336, 26);
            this.ID_textBox.TabIndex = 15;
            this.ID_textBox.Text = "ID";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePicker.Location = new System.Drawing.Point(687, 51);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(338, 26);
            this.dateTimePicker.TabIndex = 17;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.Insert_btn, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Delete_btn, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Update_btn, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Exit_btn, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 41.09174F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.60579F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.60579F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.60579F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 744);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // Insert_btn
            // 
            this.Insert_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Insert_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Insert_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Insert_btn.Location = new System.Drawing.Point(3, 308);
            this.Insert_btn.Name = "Insert_btn";
            this.Insert_btn.Size = new System.Drawing.Size(194, 117);
            this.Insert_btn.TabIndex = 9;
            this.Insert_btn.Text = "Insert";
            this.Insert_btn.UseVisualStyleBackColor = true;
            this.Insert_btn.Click += new System.EventHandler(this.Insert_btn_Click);
            // 
            // Delete_btn
            // 
            this.Delete_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Delete_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Delete_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Delete_btn.Location = new System.Drawing.Point(3, 431);
            this.Delete_btn.Name = "Delete_btn";
            this.Delete_btn.Size = new System.Drawing.Size(194, 117);
            this.Delete_btn.TabIndex = 10;
            this.Delete_btn.Text = "Delete";
            this.Delete_btn.UseVisualStyleBackColor = true;
            this.Delete_btn.Click += new System.EventHandler(this.Delete_btn_Click);
            // 
            // Update_btn
            // 
            this.Update_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Update_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Update_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Update_btn.Location = new System.Drawing.Point(3, 554);
            this.Update_btn.Name = "Update_btn";
            this.Update_btn.Size = new System.Drawing.Size(194, 117);
            this.Update_btn.TabIndex = 11;
            this.Update_btn.Text = "Update";
            this.Update_btn.UseVisualStyleBackColor = true;
            this.Update_btn.Click += new System.EventHandler(this.Update_btn_Click);
            // 
            // Exit_btn
            // 
            this.Exit_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Exit_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.Exit_btn.Location = new System.Drawing.Point(3, 677);
            this.Exit_btn.Name = "Exit_btn";
            this.Exit_btn.Size = new System.Drawing.Size(194, 64);
            this.Exit_btn.TabIndex = 12;
            this.Exit_btn.Text = "Back";
            this.Exit_btn.UseVisualStyleBackColor = true;
            this.Exit_btn.Click += new System.EventHandler(this.Exit_btn_Click);
            // 
            // Manage_Employee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 744);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Manage_Employee";
            this.Text = "Manage_Employee";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        protected System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox Password_textbox;
        private System.Windows.Forms.ComboBox Status_CB;
        private System.Windows.Forms.ComboBox Gender_CB;
        private System.Windows.Forms.TextBox CNIC_textBox;
        private System.Windows.Forms.ComboBox Role_CB;
        private System.Windows.Forms.TextBox Name_textBox;
        private System.Windows.Forms.TextBox Salary_textBox;
        private System.Windows.Forms.TextBox Contact_textBox;
        private System.Windows.Forms.TextBox Email_textBox;
        private System.Windows.Forms.TextBox ID_textBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button Insert_btn;
        private System.Windows.Forms.Button Delete_btn;
        private System.Windows.Forms.Button Update_btn;
        private System.Windows.Forms.Button Exit_btn;
        private System.Windows.Forms.TextBox FatherName_textBox;
    }
}