﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Forms.Admin;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Employee
{
    public partial class Manage_Employee : Form
    {
        public Manage_Employee()
        {
            InitializeComponent();
            DisplayDataOnDataGridView();
        }

        private void Exit_btn_Click(object sender, EventArgs e)
        {
            string b = GlobalVar.LoggerID;
            if (b.Contains("EMP") || b.Contains("emp"))
            {
                Employee_Dashboard a = new Employee_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
            else
            {
                Admin_Dashboard a = new Admin_Dashboard();
                this.Hide();
                a.ShowDialog();
            }

        }

        private void Update_btn_Click(object sender, EventArgs e)
        {
            string id = ID_textBox.Text.Trim();
            if (EmployeeDL.GetPersonIDByEmployeeID(id) >= 1)
            {
                UpdateEmployee a = new UpdateEmployee();
                this.Hide(); a.ShowDialog();
            }
            else
            {
                MessageBox.Show("Employee do not exists");
            }
            
        }

        private void Insert_btn_Click(object sender, EventArgs e)
        {
            try
            {

                string Name = Name_textBox.Text.Trim();
                int Salary = int.Parse(Salary_textBox.Text.Trim());
                string FatherName = FatherName_textBox.Text.Trim();
                string contact = Contact_textBox.Text.Trim();
                string pass = Password_textbox.Text.Trim();
                string email = Email_textBox.Text.Trim();
                DateTime joiningDate = dateTimePicker.Value.Date;
                string id = ID_textBox.Text.Trim();
                string gender = Gender_CB.SelectedItem?.ToString();
                string cnic = CNIC_textBox.Text.Trim();
                string status = Status_CB.SelectedItem?.ToString();
                string role = Role_CB.SelectedItem?.ToString();

                int g = Functions.GetLookUpValue(gender);
                int s = Functions.GetLookUpValue(status);
                int rid = Functions.GetLookUpValue(role);

                EmployeeBL b = new EmployeeBL(id, Name, email, g, contact, cnic, FatherName, pass, s, Salary, joiningDate, rid);
                if (EmployeeDL.InsertEmployeeData(b))
                {
                    MessageBox.Show("Employee added ");
                    DisplayDataOnDataGridView();
                }
                else
                {
                    MessageBox.Show("Their occured some error");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void Delete_btn_Click(object sender, EventArgs e)
        {
            string id = ID_textBox.Text.Trim();

            int i = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);

            if (EmployeeDL.DeleteEmployee(id,i))
            {
                MessageBox.Show("Deletion successfull");
                DisplayDataOnDataGridView();
            }
            else
            {
                MessageBox.Show("Their occured some error");
            }

        }

        public void DisplayDataOnDataGridView()
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();
                string query = @"SELECT s.EmployeeID, p.Name,s.Salary,s.Role, p.FatherName, p.ContactNumber, p.Email, p.CNIC, p.Gender AS Gender,p.Status AS Status, p.JoiningDate
                         FROM [dbo].[Person] p
                         INNER JOIN [dbo].[Employee] s ON p.ID = s.ID
                         INNER JOIN [dbo].[Authentication] l ON p.ID = l.ID";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

    }
}
