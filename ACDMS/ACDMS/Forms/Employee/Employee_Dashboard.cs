﻿using ACDMS.Forms.Courses;
using ACDMS.Forms.Results;
using ACDMS.Forms.Student;
using ACDMS.Forms.Attendence;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Employee
{
    public partial class Employee_Dashboard : Form
    {
        public Employee_Dashboard()
        {
            InitializeComponent();
        }

        private void LogOut_btn_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.ShowDialog();
        }

        private void MS_btn_Click(object sender, EventArgs e)
        {
            ManageStudent a = new ManageStudent();
            this.Hide();
            a.ShowDialog();
        }

        private void ME_btn_Click(object sender, EventArgs e)
        {
            Manage_Employee a = new Manage_Employee();
            this.Hide();
            a.ShowDialog();
        }

        private void MR_btn_Click(object sender, EventArgs e)
        {
            Results_Manage a = new Results_Manage();
            this.Hide(); a.ShowDialog();
        }

        private void MC_btn_Click(object sender, EventArgs e)
        {
            Course a = new Course();
            this.Hide(); a.ShowDialog();
        }

        private void MA_btn_Click(object sender, EventArgs e)
        {
            Manage_Attendance a = new Manage_Attendance();
            this.Hide(); a.ShowDialog();
        }
    }
}
