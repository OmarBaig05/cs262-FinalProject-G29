﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Employee
{
    public partial class UpdateEmployee : Form
    {
        public UpdateEmployee()
        {
            InitializeComponent();
        }

        private void Update_btn_Click(object sender, EventArgs e)
        {
            // implement logic for updation
            try
            {

                string Name = Name_textBox.Text.Trim();
                int Salary = int.Parse(Salary_textBox.Text.Trim());
                string FatherName = FatherName_textBox.Text.Trim();
                string contact = Contact_textBox.Text.Trim();
                string pass = Password_textBox.Text.Trim();
                string email = Email_textBox.Text.Trim();
                DateTime joiningDate = dateTimePicker1.Value.Date;
                string id = ID_textBox.Text.Trim();
                string gender = Gender_CB.SelectedItem?.ToString();
                string cnic = CNIC_textBox.Text.Trim();
                string status = Status_CB.SelectedItem?.ToString();
                string role = Role_CB.SelectedItem?.ToString();

                int g = Functions.GetLookUpValue(gender);
                int s = Functions.GetLookUpValue(status);
                int rid = Functions.GetLookUpValue(role);

                int j = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);

                EmployeeBL b = new EmployeeBL(id, Name, email, g, contact, cnic, FatherName, pass, s, Salary, joiningDate, rid);
                if (EmployeeDL.UpdateEmployeeData(b,j))
                {
                    MessageBox.Show("Employee Updated ");
                    togglepage();
                }
                else
                {
                    MessageBox.Show("Their occured some error");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void togglepage()
        {
            Manage_Employee a = new Manage_Employee();
            this.Hide(); a.ShowDialog();
        }
    }
}
