﻿namespace ACDMS.Forms.Employee
{
    partial class UpdateEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.FatherName_textBox = new System.Windows.Forms.TextBox();
            this.ID_textBox = new System.Windows.Forms.TextBox();
            this.Email_textBox = new System.Windows.Forms.TextBox();
            this.Contact_textBox = new System.Windows.Forms.TextBox();
            this.Salary_textBox = new System.Windows.Forms.TextBox();
            this.Name_textBox = new System.Windows.Forms.TextBox();
            this.Gender_CB = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Update_btn = new System.Windows.Forms.Button();
            this.Role_CB = new System.Windows.Forms.ComboBox();
            this.CNIC_textBox = new System.Windows.Forms.TextBox();
            this.Status_CB = new System.Windows.Forms.ComboBox();
            this.Password_textBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 374F));
            this.tableLayoutPanel1.Controls.Add(this.FatherName_textBox, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.ID_textBox, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.Email_textBox, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Contact_textBox, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Salary_textBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Name_textBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Gender_CB, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.Update_btn, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.Role_CB, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.CNIC_textBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Status_CB, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.Password_textBox, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(66, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(697, 451);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // FatherName_textBox
            // 
            this.FatherName_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FatherName_textBox.Location = new System.Drawing.Point(3, 381);
            this.FatherName_textBox.Name = "FatherName_textBox";
            this.FatherName_textBox.Size = new System.Drawing.Size(317, 26);
            this.FatherName_textBox.TabIndex = 14;
            this.FatherName_textBox.Text = "FatherName";
            this.FatherName_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ID_textBox
            // 
            this.ID_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ID_textBox.Location = new System.Drawing.Point(3, 327);
            this.ID_textBox.Name = "ID_textBox";
            this.ID_textBox.Size = new System.Drawing.Size(317, 26);
            this.ID_textBox.TabIndex = 6;
            this.ID_textBox.Text = "ID";
            this.ID_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Email_textBox
            // 
            this.Email_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Email_textBox.Location = new System.Drawing.Point(3, 165);
            this.Email_textBox.Name = "Email_textBox";
            this.Email_textBox.Size = new System.Drawing.Size(317, 26);
            this.Email_textBox.TabIndex = 3;
            this.Email_textBox.Text = "Email";
            this.Email_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Contact_textBox
            // 
            this.Contact_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contact_textBox.Location = new System.Drawing.Point(3, 111);
            this.Contact_textBox.Name = "Contact_textBox";
            this.Contact_textBox.Size = new System.Drawing.Size(317, 26);
            this.Contact_textBox.TabIndex = 2;
            this.Contact_textBox.Text = "Contact";
            this.Contact_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Salary_textBox
            // 
            this.Salary_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Salary_textBox.Location = new System.Drawing.Point(3, 57);
            this.Salary_textBox.Name = "Salary_textBox";
            this.Salary_textBox.Size = new System.Drawing.Size(317, 26);
            this.Salary_textBox.TabIndex = 1;
            this.Salary_textBox.Text = "Salary";
            this.Salary_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Name_textBox
            // 
            this.Name_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Name_textBox.Location = new System.Drawing.Point(3, 3);
            this.Name_textBox.Name = "Name_textBox";
            this.Name_textBox.Size = new System.Drawing.Size(317, 26);
            this.Name_textBox.TabIndex = 0;
            this.Name_textBox.Text = "Name";
            this.Name_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Gender_CB
            // 
            this.Gender_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Gender_CB.FormattingEnabled = true;
            this.Gender_CB.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Prefer Not To Say"});
            this.Gender_CB.Location = new System.Drawing.Point(3, 273);
            this.Gender_CB.Name = "Gender_CB";
            this.Gender_CB.Size = new System.Drawing.Size(317, 28);
            this.Gender_CB.TabIndex = 7;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePicker1.Location = new System.Drawing.Point(3, 219);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(317, 26);
            this.dateTimePicker1.TabIndex = 8;
            // 
            // Update_btn
            // 
            this.Update_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Update_btn.Location = new System.Drawing.Point(326, 381);
            this.Update_btn.Name = "Update_btn";
            this.Update_btn.Size = new System.Drawing.Size(368, 67);
            this.Update_btn.TabIndex = 9;
            this.Update_btn.Text = "Update";
            this.Update_btn.UseVisualStyleBackColor = true;
            this.Update_btn.Click += new System.EventHandler(this.Update_btn_Click);
            // 
            // Role_CB
            // 
            this.Role_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Role_CB.FormattingEnabled = true;
            this.Role_CB.Items.AddRange(new object[] {
            "Employee",
            "Teacher"});
            this.Role_CB.Location = new System.Drawing.Point(326, 3);
            this.Role_CB.Name = "Role_CB";
            this.Role_CB.Size = new System.Drawing.Size(368, 28);
            this.Role_CB.TabIndex = 10;
            // 
            // CNIC_textBox
            // 
            this.CNIC_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CNIC_textBox.Location = new System.Drawing.Point(326, 57);
            this.CNIC_textBox.Name = "CNIC_textBox";
            this.CNIC_textBox.Size = new System.Drawing.Size(368, 26);
            this.CNIC_textBox.TabIndex = 11;
            this.CNIC_textBox.Text = "CNIC";
            this.CNIC_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Status_CB
            // 
            this.Status_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Status_CB.FormattingEnabled = true;
            this.Status_CB.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.Status_CB.Location = new System.Drawing.Point(326, 111);
            this.Status_CB.Name = "Status_CB";
            this.Status_CB.Size = new System.Drawing.Size(368, 28);
            this.Status_CB.TabIndex = 12;
            // 
            // Password_textBox
            // 
            this.Password_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Password_textBox.Location = new System.Drawing.Point(326, 165);
            this.Password_textBox.Name = "Password_textBox";
            this.Password_textBox.Size = new System.Drawing.Size(368, 26);
            this.Password_textBox.TabIndex = 13;
            this.Password_textBox.Text = "Password";
            this.Password_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UpdateEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UpdateEmployee";
            this.Text = "UpdateEmployee";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox ID_textBox;
        private System.Windows.Forms.TextBox Email_textBox;
        private System.Windows.Forms.TextBox Contact_textBox;
        private System.Windows.Forms.TextBox Salary_textBox;
        private System.Windows.Forms.TextBox Name_textBox;
        private System.Windows.Forms.ComboBox Gender_CB;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button Update_btn;
        private System.Windows.Forms.ComboBox Role_CB;
        private System.Windows.Forms.TextBox CNIC_textBox;
        private System.Windows.Forms.ComboBox Status_CB;
        private System.Windows.Forms.TextBox Password_textBox;
        private System.Windows.Forms.TextBox FatherName_textBox;
    }
}