﻿namespace ACDMS.Forms.Employee
{
    partial class Employee_Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.MA_btn = new System.Windows.Forms.Button();
            this.MC_btn = new System.Windows.Forms.Button();
            this.MR_btn = new System.Windows.Forms.Button();
            this.MS_btn = new System.Windows.Forms.Button();
            this.ME_btn = new System.Windows.Forms.Button();
            this.LogOut_btn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ACADEMY = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.MA_btn, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.MC_btn, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.MR_btn, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.MS_btn, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ME_btn, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.LogOut_btn, 0, 8);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.25806F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.67742F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.67742F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.67742F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.67742F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.67742F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.67742F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.67742F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(276, 744);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // MA_btn
            // 
            this.MA_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MA_btn.Location = new System.Drawing.Point(3, 480);
            this.MA_btn.Name = "MA_btn";
            this.MA_btn.Size = new System.Drawing.Size(270, 59);
            this.MA_btn.TabIndex = 5;
            this.MA_btn.Text = "Manage Attendence";
            this.MA_btn.UseVisualStyleBackColor = true;
            this.MA_btn.Click += new System.EventHandler(this.MA_btn_Click);
            // 
            // MC_btn
            // 
            this.MC_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MC_btn.Location = new System.Drawing.Point(3, 415);
            this.MC_btn.Name = "MC_btn";
            this.MC_btn.Size = new System.Drawing.Size(270, 59);
            this.MC_btn.TabIndex = 4;
            this.MC_btn.Text = "Manage Courses";
            this.MC_btn.UseVisualStyleBackColor = true;
            this.MC_btn.Click += new System.EventHandler(this.MC_btn_Click);
            // 
            // MR_btn
            // 
            this.MR_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MR_btn.Location = new System.Drawing.Point(3, 350);
            this.MR_btn.Name = "MR_btn";
            this.MR_btn.Size = new System.Drawing.Size(270, 59);
            this.MR_btn.TabIndex = 3;
            this.MR_btn.Text = "Manage Results";
            this.MR_btn.UseVisualStyleBackColor = true;
            this.MR_btn.Click += new System.EventHandler(this.MR_btn_Click);
            // 
            // MS_btn
            // 
            this.MS_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MS_btn.Location = new System.Drawing.Point(3, 220);
            this.MS_btn.Name = "MS_btn";
            this.MS_btn.Size = new System.Drawing.Size(270, 59);
            this.MS_btn.TabIndex = 0;
            this.MS_btn.Text = "Manage Students";
            this.MS_btn.UseVisualStyleBackColor = true;
            this.MS_btn.Click += new System.EventHandler(this.MS_btn_Click);
            // 
            // ME_btn
            // 
            this.ME_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ME_btn.Location = new System.Drawing.Point(3, 285);
            this.ME_btn.Name = "ME_btn";
            this.ME_btn.Size = new System.Drawing.Size(270, 59);
            this.ME_btn.TabIndex = 1;
            this.ME_btn.Text = "Manage Employee";
            this.ME_btn.UseVisualStyleBackColor = true;
            this.ME_btn.Click += new System.EventHandler(this.ME_btn_Click);
            // 
            // LogOut_btn
            // 
            this.LogOut_btn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LogOut_btn.Location = new System.Drawing.Point(3, 704);
            this.LogOut_btn.Name = "LogOut_btn";
            this.LogOut_btn.Size = new System.Drawing.Size(270, 37);
            this.LogOut_btn.TabIndex = 7;
            this.LogOut_btn.Text = "Log Out";
            this.LogOut_btn.UseVisualStyleBackColor = true;
            this.LogOut_btn.Click += new System.EventHandler(this.LogOut_btn_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1228, 744);
            this.panel1.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(276, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(952, 744);
            this.panel3.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ACADEMY);
            this.panel4.Location = new System.Drawing.Point(155, 245);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(723, 164);
            this.panel4.TabIndex = 0;
            // 
            // ACADEMY
            // 
            this.ACADEMY.AutoSize = true;
            this.ACADEMY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ACADEMY.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.ACADEMY.Location = new System.Drawing.Point(0, 0);
            this.ACADEMY.Name = "ACADEMY";
            this.ACADEMY.Size = new System.Drawing.Size(727, 82);
            this.ACADEMY.TabIndex = 0;
            this.ACADEMY.Text = "Employee Dashboard\r\n";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(276, 744);
            this.panel2.TabIndex = 0;
            // 
            // Employee_Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 744);
            this.Controls.Add(this.panel1);
            this.Name = "Employee_Dashboard";
            this.Text = "Employee_Dashboard";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button MA_btn;
        private System.Windows.Forms.Button MC_btn;
        private System.Windows.Forms.Button MR_btn;
        private System.Windows.Forms.Button MS_btn;
        private System.Windows.Forms.Button ME_btn;
        private System.Windows.Forms.Button LogOut_btn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label ACADEMY;
        private System.Windows.Forms.Panel panel2;
    }
}