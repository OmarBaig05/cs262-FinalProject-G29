﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Forms.Employee;
using Microsoft.PowerBI.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACDMS.Forms.Admin;
using ACDMS.Supporting_Classes;
using System.Data.SqlClient;

namespace ACDMS.Forms.Enrollement
{
    public partial class Enrollement : Form
    {
        public Enrollement()
        {
            InitializeComponent();
            DisplayDataOnDataGridView();
        }

        private void Insert_btn_Click(object sender, EventArgs e)
        {
            string reg = ID_textBox.Text.Trim();
            string roll = Roll_CB.SelectedItem?.ToString();
            string subject = Subject_textBox.Text.Trim();
            string classs = Class_textBox.Text.Trim();

            int subjectID = Functions.GetSubjectID(classs, subject);

            int i = Functions.GetPersonID(reg, roll);

            EnrollmentBL a = new EnrollmentBL(i, subjectID, DateTime.Now.Date);

            if (EnrollmentsDL.InsertEnrollment(a))
            {
                MessageBox.Show("Added");
                DisplayDataOnDataGridView();
            }
            else
            {
                MessageBox.Show("Error");
            }
        }

        private void Exit_btn_Click(object sender, EventArgs e)
        {
            if(GlobalVar.LoggerID.Substring(5,3) == "EMP" || GlobalVar.LoggerID.Substring(5, 3) == "emp")
            {
                Employee_Dashboard a = new Employee_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
            else
            {
                Admin_Dashboard a = new Admin_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
            
        }

        private void Delete_btn_Click(object sender, EventArgs e)
        {
            string reg = ID_textBox.Text.Trim();
            string roll = Roll_CB.SelectedItem?.ToString();
            string subject = Subject_textBox.Text.Trim();
            string classs = Class_textBox.Text.Trim();

            int subjectID = Functions.GetSubjectID(classs, subject);

            int i = Functions.GetPersonID(reg, roll);

            EnrollmentBL a = new EnrollmentBL(i, subjectID, DateTime.Now.Date);

            int j = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);

            if (EnrollmentsDL.DeleteEnrollment(a,j))
            {
                MessageBox.Show("Added");
                DisplayDataOnDataGridView();
            }
            else
            {
                MessageBox.Show("Error");
            }
        }

        public void DisplayDataOnDataGridView()
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();
                string query = @"SELECT EnrollmentDate,PersonID,SubjectID from Enrollments";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
    }
}
