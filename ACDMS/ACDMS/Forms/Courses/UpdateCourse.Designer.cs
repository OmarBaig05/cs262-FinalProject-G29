﻿namespace ACDMS.Forms.Courses
{
    partial class UpdateCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.Subject_textBox = new System.Windows.Forms.TextBox();
            this.Class_textBox = new System.Windows.Forms.TextBox();
            this.Discipline_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SubjectStatus_CB = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 326F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Subject_textBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Class_textBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Discipline_textBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.SubjectStatus_CB, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(140, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(494, 452);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(3, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 90);
            this.label3.TabIndex = 6;
            this.label3.Text = "Discipline";
            // 
            // Subject_textBox
            // 
            this.Subject_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Subject_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Subject_textBox.Location = new System.Drawing.Point(171, 3);
            this.Subject_textBox.Name = "Subject_textBox";
            this.Subject_textBox.Size = new System.Drawing.Size(320, 35);
            this.Subject_textBox.TabIndex = 0;
            this.Subject_textBox.Text = "Subject";
            this.Subject_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Class_textBox
            // 
            this.Class_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Class_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Class_textBox.Location = new System.Drawing.Point(171, 93);
            this.Class_textBox.Name = "Class_textBox";
            this.Class_textBox.Size = new System.Drawing.Size(320, 35);
            this.Class_textBox.TabIndex = 1;
            this.Class_textBox.Text = "Class";
            this.Class_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Discipline_textBox
            // 
            this.Discipline_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Discipline_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Discipline_textBox.Location = new System.Drawing.Point(171, 183);
            this.Discipline_textBox.Name = "Discipline_textBox";
            this.Discipline_textBox.Size = new System.Drawing.Size(320, 35);
            this.Discipline_textBox.TabIndex = 2;
            this.Discipline_textBox.Text = "Discipline";
            this.Discipline_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 90);
            this.label1.TabIndex = 4;
            this.label1.Text = "Subject";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(3, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 90);
            this.label2.TabIndex = 5;
            this.label2.Text = "Class";
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(171, 363);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(320, 86);
            this.button1.TabIndex = 3;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SubjectStatus_CB
            // 
            this.SubjectStatus_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SubjectStatus_CB.FormattingEnabled = true;
            this.SubjectStatus_CB.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.SubjectStatus_CB.Location = new System.Drawing.Point(171, 273);
            this.SubjectStatus_CB.Name = "SubjectStatus_CB";
            this.SubjectStatus_CB.Size = new System.Drawing.Size(320, 28);
            this.SubjectStatus_CB.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(3, 270);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 90);
            this.label4.TabIndex = 17;
            this.label4.Text = "Status";
            // 
            // UpdateCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UpdateCourse";
            this.Text = "UpdateCourse";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox Discipline_textBox;
        private System.Windows.Forms.TextBox Class_textBox;
        private System.Windows.Forms.TextBox Subject_textBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox SubjectStatus_CB;
        private System.Windows.Forms.Label label4;
    }
}