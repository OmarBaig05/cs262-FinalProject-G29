﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Forms.Admin;
using ACDMS.Forms.Employee;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Courses
{
    public partial class Course : Form
    {
        public Course()
        {
            InitializeComponent();
        }

        private void Update_btn_Click(object sender, EventArgs e)
        {
            try
            {
                GlobalVar.SubjectIDForUpdation = int.Parse(ID_textBox.Text.Trim());
                UpdateCourse a = new UpdateCourse();
                this.Hide();
                a.ShowDialog();
            }
            catch
            {
                Console.WriteLine("Error Occured");
                MessageBox.Show("Type the valid SubjectID");
            }

        }

        private void Exit_btn_Click(object sender, EventArgs e)
        {
            string b = GlobalVar.LoggerID;
            if (b.Contains("EMP") || b.Contains("emp"))
            {
                Employee_Dashboard a = new Employee_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
            else
            {
                Admin_Dashboard a = new Admin_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
        }

        private void Insert_btn_Click(object sender, EventArgs e)
        {
            string subject = Subject_textBox.Text.Trim();
            string classs = Class_textBox.Text.Trim();
            string discipine = Discipline_textBox.Text.Trim();
            string status = SubjectStatus_CB.SelectedItem?.ToString();
            int i = Functions.GetLookUpValue(status);

            CoursesBL a = new CoursesBL(subject, classs, i, discipine);

            if (CoursesDL.InsertClassData(a))
            {
                MessageBox.Show("Data Inserted Successfuly");
            }
            else
            {
                MessageBox.Show("Their is some error occured");
            }
        }

        private void Delete_btn_Click(object sender, EventArgs e)
        {
            string subject = Subject_textBox.Text.Trim();
            string classs = Class_textBox.Text.Trim();

            int j = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);

            if (CoursesDL.DeleteClassAndSubject(classs, subject, j))
            {
                MessageBox.Show("Data Deleted Successfuly");
            }
            else
            {
                MessageBox.Show("Their is some error occured");
            }

        }

        public void DisplayDataOnDataGridView()
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();
                string query = @"SELECT 
                            c.ClassID, 
                            c.Name AS ClassName, 
                            c.Discipline, 
                            s.SubjectID, 
                            s.Name AS SubjectName, 
                            s.Status
                        FROM 
                            [dbo].[Class] c
                        JOIN 
                            [dbo].[Subject] s ON c.ClassID = s.ClassID";

                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

    }
}
