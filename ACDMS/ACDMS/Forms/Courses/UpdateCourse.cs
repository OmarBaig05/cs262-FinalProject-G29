﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Courses
{
    public partial class UpdateCourse : Form
    {
        public UpdateCourse()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string subject = Subject_textBox.Text.Trim();
            string classs = Class_textBox.Text.Trim();
            string discipine = Discipline_textBox.Text.Trim();
            string status = SubjectStatus_CB.SelectedItem?.ToString();
            int i = Functions.GetLookUpValue(status);

            CoursesBL a = new CoursesBL(subject, classs, i, discipine);

            (string b, string c) = CoursesDL.GetClassAndSubjectName(GlobalVar.SubjectIDForUpdation);
            int j = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);

            if (CoursesDL.UpdateClassAndSubject(b,c,a,j))
            {
                MessageBox.Show("Data Inserted Successfuly");
                ToggelPage();
            }
            else
            {
                MessageBox.Show("Their is some error occured");
                ToggelPage();
            }
        }

        private void ToggelPage()
        {
            Course course = new Course();
            this.Hide();
            course.ShowDialog();
        }


    }
}
