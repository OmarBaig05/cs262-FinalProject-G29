﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Forms.Admin;
using ACDMS.Forms.Employee;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Forms.Student
{
    public partial class ManageStudent : Form
    {
        public ManageStudent()
        {
            InitializeComponent();
            DisplayDataOnDataGridView();
        }

        private void Insert_btn_Click(object sender, EventArgs e)
        {

            string Name = Name_textBox.Text.Trim();
            string FatherName= FatherName_textBox.Text.Trim();
            //string contact = "00000000000";
            string contact = Contact_textBox.Text.Trim();
            //string email = "email@student.com";
            string email = Email_textBox.Text.Trim();
            DateTime joiningDate = dateTimePicker.Value.Date;
            //string registrationNo = "2022-10-123";
            string registrationNo = RegNo_textBox.Text.Trim();
            //string cnic = "00000-0000000-1";
            string cnic = CNIC_textBox.Text.Trim();
            string gender = Gender_CB.SelectedItem?.ToString();
            //string gender = "Male";
            string status = Status_CB.SelectedItem?.ToString();
            //string status = "Active";

            int g = Functions.GetLookUpValue(gender);
            int s = Functions.GetLookUpValue(status);
            Console.WriteLine(g);

            StudentBL std = new StudentBL(registrationNo, Name, email, contact, cnic, FatherName, joiningDate, g, s);

            if(StudentDL.InsertStudentData(std)){
                MessageBox.Show("Inserted Successfully");
                DisplayDataOnDataGridView();
            }
            else
            {
                MessageBox.Show("An issue occured, try again");
            }
            
            
        }


        private void Exit_btn_Click(object sender, EventArgs e)
        {

            string b = GlobalVar.LoggerID;
            if (b.Contains("EMP") || b.Contains("emp"))
            {
                Employee_Dashboard a = new Employee_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
            else
            {
                Admin_Dashboard a = new Admin_Dashboard();
                this.Hide();
                a.ShowDialog();
            }
        }

        private void Delete_btn_Click(object sender, EventArgs e)
        {
            string registrationNo = RegNo_textBox.Text.Trim();
            int i = Functions.GetPersonID(GlobalVar.LoggerID, GlobalVar.RoleOfLogger);
            if (StudentDL.DeleteStudent(registrationNo,i))
            {
                MessageBox.Show("Deletion successful");
                DisplayDataOnDataGridView();
            }
            else
            {
                MessageBox.Show("Some issue occured, try again");
            }
        }

        public void DisplayDataOnDataGridView()
        {
            try
            {
                var connection = Configuration.getInstance().getConnection();
                string query = @"SELECT s.StudentID, p.Name, p.FatherName, p.ContactNumber, p.Email, p.CNIC, p.Gender AS Gender,p.Status AS Status, p.JoiningDate
                         FROM [dbo].[Person] p
                         INNER JOIN [dbo].[Student] s ON p.ID = s.ID";
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
    }
}
