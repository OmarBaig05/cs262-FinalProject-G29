﻿namespace ACDMS
{
    partial class SignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CNIC_textBox = new System.Windows.Forms.TextBox();
            this.Email_textBox = new System.Windows.Forms.TextBox();
            this.Gender_CB = new System.Windows.Forms.ComboBox();
            this.FirstName_textBox = new System.Windows.Forms.TextBox();
            this.RegNo_textBox = new System.Windows.Forms.TextBox();
            this.Password_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Status_CB = new System.Windows.Forms.ComboBox();
            this.SignUp_btn = new System.Windows.Forms.Button();
            this.Father_textBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label8.Location = new System.Drawing.Point(322, 40);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(200, 31);
            this.label8.TabIndex = 5;
            this.label8.Text = "SIGN UP Menu";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(2, 280);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(212, 35);
            this.label10.TabIndex = 27;
            this.label10.Text = "Status";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(2, 245);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(212, 35);
            this.label9.TabIndex = 25;
            this.label9.Text = "Contact No.";
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(218, 247);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(245, 20);
            this.textBox1.TabIndex = 24;
            this.textBox1.Text = "Contact no.";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(2, 210);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(212, 35);
            this.label7.TabIndex = 22;
            this.label7.Text = "ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(2, 175);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(212, 35);
            this.label6.TabIndex = 21;
            this.label6.Text = "Status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(2, 140);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(212, 35);
            this.label5.TabIndex = 20;
            this.label5.Text = "Gender";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(2, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(212, 35);
            this.label4.TabIndex = 19;
            this.label4.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(2, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(212, 35);
            this.label3.TabIndex = 18;
            this.label3.Text = "Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(2, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(212, 35);
            this.label2.TabIndex = 17;
            this.label2.Text = "CNIC";
            // 
            // CNIC_textBox
            // 
            this.CNIC_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CNIC_textBox.Location = new System.Drawing.Point(218, 37);
            this.CNIC_textBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.CNIC_textBox.Name = "CNIC_textBox";
            this.CNIC_textBox.Size = new System.Drawing.Size(245, 20);
            this.CNIC_textBox.TabIndex = 11;
            this.CNIC_textBox.Text = "CNIC";
            this.CNIC_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Email_textBox
            // 
            this.Email_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Email_textBox.Location = new System.Drawing.Point(218, 107);
            this.Email_textBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Email_textBox.Name = "Email_textBox";
            this.Email_textBox.Size = new System.Drawing.Size(245, 20);
            this.Email_textBox.TabIndex = 13;
            this.Email_textBox.Text = "Email";
            this.Email_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Gender_CB
            // 
            this.Gender_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Gender_CB.FormattingEnabled = true;
            this.Gender_CB.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Prefer Not To Say"});
            this.Gender_CB.Location = new System.Drawing.Point(218, 142);
            this.Gender_CB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Gender_CB.Name = "Gender_CB";
            this.Gender_CB.Size = new System.Drawing.Size(245, 21);
            this.Gender_CB.TabIndex = 10;
            // 
            // FirstName_textBox
            // 
            this.FirstName_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FirstName_textBox.Location = new System.Drawing.Point(218, 2);
            this.FirstName_textBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FirstName_textBox.Name = "FirstName_textBox";
            this.FirstName_textBox.Size = new System.Drawing.Size(245, 20);
            this.FirstName_textBox.TabIndex = 0;
            this.FirstName_textBox.Text = "Name";
            this.FirstName_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RegNo_textBox
            // 
            this.RegNo_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RegNo_textBox.Location = new System.Drawing.Point(218, 212);
            this.RegNo_textBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RegNo_textBox.Name = "RegNo_textBox";
            this.RegNo_textBox.Size = new System.Drawing.Size(245, 20);
            this.RegNo_textBox.TabIndex = 6;
            this.RegNo_textBox.Text = "ID (Format: 2022-ADM-01)";
            this.RegNo_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Password_textBox
            // 
            this.Password_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Password_textBox.Location = new System.Drawing.Point(218, 72);
            this.Password_textBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Password_textBox.Name = "Password_textBox";
            this.Password_textBox.Size = new System.Drawing.Size(245, 20);
            this.Password_textBox.TabIndex = 14;
            this.Password_textBox.Text = "Password";
            this.Password_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(212, 35);
            this.label1.TabIndex = 16;
            this.label1.Text = "Name";
            // 
            // Status_CB
            // 
            this.Status_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Status_CB.FormattingEnabled = true;
            this.Status_CB.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.Status_CB.Location = new System.Drawing.Point(218, 177);
            this.Status_CB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Status_CB.Name = "Status_CB";
            this.Status_CB.Size = new System.Drawing.Size(245, 21);
            this.Status_CB.TabIndex = 23;
            // 
            // SignUp_btn
            // 
            this.SignUp_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SignUp_btn.Location = new System.Drawing.Point(218, 317);
            this.SignUp_btn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.SignUp_btn.Name = "SignUp_btn";
            this.SignUp_btn.Size = new System.Drawing.Size(245, 33);
            this.SignUp_btn.TabIndex = 26;
            this.SignUp_btn.Text = "Sign Up";
            this.SignUp_btn.UseVisualStyleBackColor = true;
            this.SignUp_btn.Click += new System.EventHandler(this.SignUp_btn_Click_2);
            // 
            // Father_textBox
            // 
            this.Father_textBox.Location = new System.Drawing.Point(218, 282);
            this.Father_textBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Father_textBox.Name = "Father_textBox";
            this.Father_textBox.Size = new System.Drawing.Size(245, 20);
            this.Father_textBox.TabIndex = 28;
            this.Father_textBox.Text = "Father Name";
            this.Father_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 249F));
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.CNIC_textBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Email_textBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.Gender_CB, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.FirstName_textBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.RegNo_textBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.Password_textBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Status_CB, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.SignUp_btn, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.Father_textBox, 1, 8);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(177, 92);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999433F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999433F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999433F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999433F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999433F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999433F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999433F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00244F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.00244F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.9991F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(465, 352);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // SignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 484);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SignUp";
            this.Text = "SignUp";
            this.Load += new System.EventHandler(this.SignUp_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CNIC_textBox;
        private System.Windows.Forms.TextBox Email_textBox;
        private System.Windows.Forms.ComboBox Gender_CB;
        private System.Windows.Forms.TextBox FirstName_textBox;
        private System.Windows.Forms.TextBox RegNo_textBox;
        private System.Windows.Forms.TextBox Password_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Status_CB;
        private System.Windows.Forms.Button SignUp_btn;
        private System.Windows.Forms.TextBox Father_textBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}