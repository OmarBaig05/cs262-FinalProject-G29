﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Forms.Admin;
using ACDMS.Forms.Attendence;
using ACDMS.Forms.Employee;
using ACDMS.Supporting_Classes;
using Microsoft.PowerBI.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void Submit_btn_Click(object sender, EventArgs e)
        {
            string email = Email_textBox.Text;
            string pass = Password_textBox.Text;
            (string ID, string role) = Functions.ValidateCredentials(email, pass); 
            //(string ID, string role) = Functions.ValidateCredentials("email@example.com", "password123@");
            GlobalVar.LoggerID = ID;
            GlobalVar.RoleOfLogger = role;
            Console.WriteLine("ID", ID);
            Console.WriteLine("role", role);

            if (!string.IsNullOrEmpty(ID))
            {
                if (role == "Admin")
                {
                    Admin_Dashboard a = new Admin_Dashboard();
                    a.ShowDialog();
                    this.Hide();
                }
                else if (role == "Employee")
                {
                    string employeeType = ID.Substring(4, 3);
                    Console.WriteLine(employeeType);
                    int id = Functions.GetPersonID(ID, "Employee");
                    DateTime currentDate = DateTime.Now.Date;
                    int Status = Functions.GetLookUpValue("Present");

                    if (employeeType == "EMP" || employeeType == "emp")
                    {
                        AttendanceBL z = new AttendanceBL(id, "Employee", currentDate, Status);
                        AttendanceDL.MarkAttendance(z);

                        Employee_Dashboard a = new Employee_Dashboard();
                        this.Hide();
                        a.ShowDialog();
                    }
                    else if (employeeType == "tch" || employeeType == "TCH")
                    {
                        AttendanceBL a = new AttendanceBL(id,"Teacher",currentDate,Status);

                        AttendanceDL.MarkAttendance(a);

                        MessageBox.Show("Your Attendance is marked");

                    }
                }
                else
                {
                    MessageBox.Show("Invalid Credentials");
                }
            }
            else
            {
                MessageBox.Show("User Not Found");
            }
        }

        private void SignUp_btn_Click(object sender, EventArgs e)
        {
            Admin_Dashboard a = new Admin_Dashboard();
            //Manage_Attendance a = new Manage_Attendance();
            //SignUp a = new SignUp();
            //ACDMS.Forms.Admin.Reports a = new ACDMS.Forms.Admin.Reports();
            this.Hide();
            a.ShowDialog();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
