﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class DuesBL
    {
        public string StudentID { get; set; }
        public decimal Amount { get; set; }
        public DateTime DueDate { get; set; }
        public int PaymentStatus { get; set; }
        public int SubmissionStatus { get; set; }

        public DuesBL(string studentID, decimal amount, DateTime dueDate, int paymentStatus, int submissionStatus, int paymentStatuss)
        {
            StudentID = studentID;
            Amount = amount;
            DueDate = dueDate;
            PaymentStatus = paymentStatuss;
            SubmissionStatus = submissionStatus;
        }
    }
}
