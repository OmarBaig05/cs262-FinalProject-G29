﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class CoursesBL
    {
        public string Subject { get; set; }
        public int Status { get; set; }
        public string Class { get; set; }
        public string Discipline { get; set; }
        public CoursesBL(string subject, string classe, int status, string discipline)
        {
            Subject = subject;
            Class = classe;
            Status = status;
            Discipline = discipline;
        }
    }
}
