﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class StudentBL
    {
        public string StudentID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string CNIC { get; set; }
        public string FatherName { get; set; }
        public int Gender { get; set; }
        public int Status { get; set; }
        public DateTime JoiningDate { get; set; }

        public StudentBL(string studentID, string name, string email, string contactNumber, string cnic, string fatherName, DateTime joiningDate, int gender, int status)
        {
            StudentID = studentID;
            Name = name;
            Email = email;
            ContactNumber = contactNumber;
            CNIC = cnic;
            FatherName = fatherName;
            JoiningDate = joiningDate;
            Gender = this.Gender;
            Status = this.Status;
        }
    }
}
