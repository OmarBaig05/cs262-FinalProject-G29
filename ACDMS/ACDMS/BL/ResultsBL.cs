﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class ResultsBL
    {
        public string StudentID { get; set; }
        public string TeacherID { get; set; }
        public int SubjectID { get; set; }
        public decimal ObtainedMarks { get; set; }
        public decimal TotalMarks { get; set; }
        public DateTime ResultDate { get; set; }

        public ResultsBL( string studentID, string teacherID, int subjectID, decimal obtainedMarks, decimal totalMarks, DateTime resultDate)
        {
            StudentID = studentID;
            TeacherID = teacherID;
            SubjectID = subjectID;
            ObtainedMarks = obtainedMarks;
            TotalMarks = totalMarks;
            ResultDate = resultDate;
        }
    }
}
