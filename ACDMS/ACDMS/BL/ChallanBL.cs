﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class ChallanBL
    {
        public int ChallanID { get; set; }
        public int DuesID { get; set; }
        public string ChallanNumber { get; set; }
        public DateTime DatePaid { get; set; }

        public ChallanBL(int challanID, int duesID, string challanNumber, DateTime datePaid)
        {
            ChallanID = challanID;
            DuesID = duesID;
            ChallanNumber = challanNumber;
            DatePaid = datePaid;
        }
    }
}
