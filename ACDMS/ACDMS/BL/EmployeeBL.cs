﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class EmployeeBL
    {
        public string EmployeeID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string CNIC { get; set; }
        public string FatherName { get; set; }
        public string Password { get; set; }
        public int Gender { get; set; }
        public int Status { get; set; }
        public int RoleID { get; set; }
        public decimal EmployeeSalary { get; set; }
        public DateTime JoiningDate { get; set; }

        public EmployeeBL(string employeeID, string name, string email, int gender, string contact, string Cnic, string fathername, string password, int status, decimal employeeSalary, DateTime joining, int roleID)
        {
            EmployeeID = employeeID;
            Name = name;
            Email = email;
            Gender = gender;
            ContactNumber = contact;
            CNIC = Cnic;
            FatherName = fathername;
            Password = password;
            Status = status;
            EmployeeSalary = employeeSalary;
            JoiningDate = joining;
            RoleID = roleID;
        }
    }
}
