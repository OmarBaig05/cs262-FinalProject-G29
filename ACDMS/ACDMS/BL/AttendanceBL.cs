﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class AttendanceBL
    {
        public int PersonID { get; set; }
        public string AttendanceType { get; set; }
        public DateTime AttendanceDate { get; set; }
        public int AttendanceStatus { get; set; }

        public AttendanceBL(int personID, string attendanceType, DateTime attendanceDate, int attendanceStatus)
        {
            PersonID = personID;
            AttendanceType = attendanceType;
            AttendanceDate = attendanceDate;
            AttendanceStatus = attendanceStatus;
        }
    }
}
