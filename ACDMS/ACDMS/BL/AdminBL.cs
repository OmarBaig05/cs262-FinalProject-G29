﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class AdminBL
    {
        public string AdminID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string CNIC { get; set; }
        public string FatherName { get; set; }
        public string Password { get; set; }
        public int Gender { get; set; }
        public int Status { get; set; }
        public DateTime JoiningDate { get; set; }

        public AdminBL(string adminID, string name, string email, int gender, string contact, string Cnic, string fathername, string password, int status, DateTime joining)
        {
            AdminID = adminID;
            Name = name;
            Email = email;
            Gender = gender;
            ContactNumber = contact;
            CNIC = Cnic;
            FatherName = fathername;
            Password = password;
            Status = status;
            JoiningDate = joining;

        }
    }
}
