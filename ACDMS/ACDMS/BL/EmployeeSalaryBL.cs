﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class EmployeeSalaryBL
    {
        public string EmployeeID { get; set; }
        public decimal EmployeeSalary { get; set; }
        public int Status { get; set; }
        public DateTime DateOfPaid { get; set; }

        public EmployeeSalaryBL(string employeeID, decimal employeeSalary, int status, DateTime dateOfPaid)
        {
            EmployeeID = employeeID;
            EmployeeSalary = employeeSalary;
            Status = status;
            DateOfPaid = dateOfPaid;
        }
    }
}
