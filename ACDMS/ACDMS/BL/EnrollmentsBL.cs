﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.BL
{
    internal class EnrollmentBL
    {
        public int PersonID { get; set; }
        public int SubjectID { get; set; }

        public DateTime DateTime { get; set; }

        public EnrollmentBL(int personID, int subjectID, DateTime dateTime)
        {
            PersonID = personID;
            SubjectID = subjectID;
            DateTime = dateTime;
        }
    }
}
