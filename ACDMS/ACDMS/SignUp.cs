﻿using ACDMS.BL;
using ACDMS.DL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS
{
    public partial class SignUp : Form
    {
        public SignUp()
        {
            InitializeComponent();
        }

        private void SignUp_btn_Click_1(object sender, EventArgs e)
        {


        }

        private void TogglePage()
        {
            Form1 a = new Form1();
            this.Hide();
            a.ShowDialog();
        }

        private void SignUp_btn_Click(object sender, EventArgs e)
        {
            
        }

        private void SignUp_btn_Click_2(object sender, EventArgs e)
        {
            // implementation required
            string id = RegNo_textBox.Text.Trim();
            string email = Email_textBox.Text.Trim();
            string name = FirstName_textBox.Text.Trim();
            string pass = Password_textBox.Text.Trim();
            string CNIC = CNIC_textBox.Text.Trim();
            string contact = textBox1.Text.Trim();
            string father = Father_textBox.Text.Trim();
            string Gender = Gender_CB.SelectedItem?.ToString();
            string Status = Status_CB.SelectedItem?.ToString();
            int g = Functions.GetLookUpValue(Gender);
            int s = Functions.GetLookUpValue(Status);

            AdminBL a = new AdminBL(id, name, email, g, contact, CNIC, father, pass, s, DateTime.Now);

            if (AdminDL.InsertAdminData(a))
            {
                MessageBox.Show(id + " Added!");

            }

            TogglePage();
        }

        private void SignUp_Load(object sender, EventArgs e)
        {

        }
    }
}
