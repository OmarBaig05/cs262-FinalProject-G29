﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ACDMS.Supporting_Classes
{
    public class PowerBIAuthentication
    {
        private const string clientId = "769af354-2336-4619-8fb7-9d6b5b013fe4";
        private const string clientSecret = "abb3f764-1ac4-4d85-af65-875403ea8be3";
        private const string tenantId = "1d7aa2e9-3a1a-4af4-9873-31cc6674969e";

        public async Task<string> GetAccessToken()
        {
            using (HttpClient client = new HttpClient())
            {
                var tokenEndpoint = $"https://login.microsoftonline.com/{tenantId}/oauth2/v2.0/token";

                var requestContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("client_id", clientId),
                    new KeyValuePair<string, string>("client_secret", clientSecret),
                    new KeyValuePair<string, string>("scope", "https://analysis.windows.net/powerbi/api/.default"),
                    new KeyValuePair<string, string>("grant_type", "client_credentials")
                });

                using (var response = await client.PostAsync(tokenEndpoint, requestContent))
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var accessToken = responseContent.Split('"')[3];
                    return accessToken;
                }
            }
        }
    }
}