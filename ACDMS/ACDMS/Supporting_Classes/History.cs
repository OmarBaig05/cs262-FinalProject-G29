﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.Supporting_Classes
{
    internal class History
    {
        public int RecordID { get; set; }
        public string EmployeeID { get; set; }
        public string ChangedEntity { get; set; }
        public string ChangeType { get; set; }
        public DateTime ChangeTime { get; set; }

        public History(int recordID, string employeeID, string changedEntity, string changeType, DateTime changeTime)
        {
            RecordID = recordID;
            EmployeeID = employeeID;
            ChangedEntity = changedEntity;
            ChangeType = changeType;
            ChangeTime = changeTime;
        }

        public static void LogChanges(SqlConnection connection, SqlTransaction transaction, int ID, string changeMessage, string changedEntity, int TupleID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "INSERT INTO History (ID, ChangedEntity, ChangeType,TupleID, ChangeTime) " +
                                      "VALUES (@id, @ChangedEntity, @ChangeType,@TupleID, GETDATE())";
                command.Parameters.AddWithValue("@id", ID);
                command.Parameters.AddWithValue("@ChangedEntity", changedEntity);
                command.Parameters.AddWithValue("@ChangeType", changeMessage);
                command.Parameters.AddWithValue("@TupleID", TupleID);
                command.ExecuteNonQuery();
            }
        }
    }
}
