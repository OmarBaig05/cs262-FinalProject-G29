﻿using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.Models;
using Microsoft.Rest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ACDMS.Supporting_Classes
{
    public class PowerBIManager
    {
        private readonly string pushUrl; // Replace with your complete push URL, including dataset key

        public PowerBIManager()
        {
            // Replace with your complete push URL, including dataset key
            
        }

        public async Task<bool> PushAttendanceTableToPowerBI(DataTable dataTable, string tableName)
        {
            string pushUrl = "https://api.powerbi.com/beta/1d7aa2e9-3a1a-4af4-9873-31cc6674969e/datasets/6f6e0208-2a77-4206-9870-cdc74b70cc69/rows?experience=power-bi&clientSideAuth=0&key=xuXyelFkNwgsO7lq8TvPMpGjvM0WHVUyDqj%2FMwA%2FjR4W0jLRiWeSeZMn7oq4eqAmYdjuthBALyjYJwbClC7j9w%3D%3D";
            try
            {
                using (var client = new HttpClient())
                {
                    // Convert DataTable to JSON string
                    string jsonData = JsonConvert.SerializeObject(dataTable.Rows.Cast<DataRow>().Select(r => r.ItemArray.Select(i => (object)i.ToString()).ToList<object>()).ToList<object>());

                    StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync(pushUrl, content);
                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("Data pushed successfully to Power BI.");
                        return true;
                    }


                    if (!response.IsSuccessStatusCode)
                    {
                        string error = await response.Content.ReadAsStringAsync();
                        throw new Exception($"Error pushing data: {error}");
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return false;
            }
        }


        public async Task<bool> PushAttendanceDailyTableToPowerBI(DataTable dataTable, string tableName)
        {
            string pushUrl = "https://api.powerbi.com/beta/1d7aa2e9-3a1a-4af4-9873-31cc6674969e/datasets/49c3d085-762e-4720-b721-448a57de7524/rows?experience=power-bi&clientSideAuth=0&key=oLlcUGrNpEmzaQg1MkZWYqVwovY8pEy%2FGj13DqmkOmg2jbB31qeQhZbeDKY3Vku3lp7PIrooZGwZwldyV4jWdg%3D%3D";
            
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        // Convert DataTable to JSON string
                        string jsonData = JsonConvert.SerializeObject(dataTable.Rows.Cast<DataRow>().Select(r => r.ItemArray.Select(i => (object)i.ToString()).ToList<object>()).ToList<object>());

                        // Log the request body (JSON data)
                        Console.WriteLine($"Request Body (JSON): {jsonData}");

                        StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                        HttpResponseMessage response = await client.PostAsync(pushUrl, content);

                        if (!response.IsSuccessStatusCode)
                        {
                            string error = await response.Content.ReadAsStringAsync();
                            string errorMessage = $"Error pushing data: Status code {response.StatusCode} - {error}";
                            throw new Exception(errorMessage);
                        }

                        return true; // Data pushed successfully
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                    return false;
                }
            }
        }


        public async Task<bool> PushPNLTableToPowerBI(DataTable dataTable, string tableName)
        {
            string pushUrl = "https://api.powerbi.com/beta/1d7aa2e9-3a1a-4af4-9873-31cc6674969e/datasets/a3e45b33-c5f6-4fd2-9445-f3c1dcbcaa9a/rows?experience=power-bi&clientSideAuth=0&key=q1rq%2FHKrKy0GaRxdi0Ikw6DlV9cku3Y3uH8J5SW3G39zOVcAq%2BEkPpHOqDmt5ZOWg5lamZXf%2BDLSN2BUb1w0Lg%3D%3D";
            try
            {
                using (var client = new HttpClient())
                {
                    // Convert DataTable to JSON string
                    string jsonData = JsonConvert.SerializeObject(dataTable.Rows.Cast<DataRow>().Select(r => r.ItemArray.Select(i => (object)i.ToString()).ToList<object>()).ToList<object>());

                    StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync(pushUrl, content);
                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("Data pushed successfully to Power BI.");
                        return true;
                    }


                    if (!response.IsSuccessStatusCode)
                    {
                        string error = await response.Content.ReadAsStringAsync();
                        throw new Exception($"Error pushing data: {error}");
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return false;
            }
        }
    }
}
