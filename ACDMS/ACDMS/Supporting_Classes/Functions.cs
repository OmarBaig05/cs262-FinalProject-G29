﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.Supporting_Classes
{
    internal class Functions
    {

        public static int GetLookUpValue(string value)
        {
            int lookupID = -1; // Initialize with a default value indicating failure

            try
            {
                using (var connection = Configuration.getInstance().getConnection())
                {
                    string query = "SELECT ID FROM Lookup WHERE Value = @Value";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Value", value);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                lookupID = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            return lookupID;
        }


            public static int GetSubjectID(string className, string subjectName)
            {
                // Initialize the SubjectID to -1 indicating failure
                int subjectID = -1;

                // Connection string (replace with your actual connection string)
                string connectionString = "Data Source=YourServer;Initial Catalog=ACDMS;Integrated Security=True";

                // SQL query to retrieve SubjectID
                string query = "SELECT Subject.SubjectID " +
                               "FROM Subject INNER JOIN Class ON Subject.ClassID = Class.ClassID " +
                               "WHERE Subject.SubjectName = @SubjectName AND Class.ClassName = @ClassName";

                // Create connection and command objects
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Add parameters for className and subjectName
                    command.Parameters.AddWithValue("@ClassName", className);
                    command.Parameters.AddWithValue("@SubjectName", subjectName);

                    try
                    {
                        // Open connection
                        connection.Open();

                        // Execute query and retrieve SubjectID
                        object result = command.ExecuteScalar();

                        if (result != null && result != DBNull.Value)
                        {
                            // Convert the result to an integer
                            subjectID = Convert.ToInt32(result);
                        }
                        else
                        {
                            MessageBox.Show("No subject found for the given class and subject name.");
                        }
                    }
                    catch (Exception ex)
                    {
                        // Handle exceptions
                        Console.WriteLine("An error occurred: " + ex.Message);
                    }
                }

                return subjectID;
            }


            public static int GetPersonID(string id, string type)
            {
                string tableName = type == "Student" ? "Student" : "Employee";
                string idColumn = type == "Student" ? "StudentID" : "EmployeeID";
                int status = GetLookUpValue("Active");
                int personID = -1;

                string connectionString = @"Data Source=(local);Initial Catalog=ACDMS;Integrated Security=True";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    // SQL query to retrieve PersonID from the respective table with active status
                    string query = $"SELECT ID FROM {tableName} WHERE {idColumn} = @ID";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@ID", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                personID = reader.GetInt32(0);
                            }
                        }
                    }
                }

                return personID;
            }

            public static int GetResultIDByStudentIDAndSubjectID(string studentID, int subjectID, string teacherID)
            {
                int resultID = -1; // Initialize with a default value indicating failure

                string query = "SELECT ResultID FROM Results WHERE StudentID = @StudentID AND SubjectID = @SubjectID and TeacherID = @TeacherID";

                using(var connection = Configuration.getInstance().getConnection())
            {

            

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@StudentID", studentID);
                    command.Parameters.AddWithValue("@SubjectID", subjectID);
                    command.Parameters.AddWithValue("@TeacherID", teacherID);

                    // Execute the command and get the result
                    object result = command.ExecuteScalar();

                    // Check if the result is not null and convert it to an integer
                    if (result != null && result != DBNull.Value)
                    {
                        resultID = Convert.ToInt32(result);
                    }
                }
            }

            return resultID;
            }

            public static int GetSubjectIDByClassNameAndDisciplineAndSubjectName(SqlConnection connection, SqlTransaction transaction, string className, string discipline, string subjectName)
            {
                int subjectID = -1; // Initialize with a default value indicating failure

                string query = "SELECT s.SubjectID FROM Subject s " +
                               "JOIN Class c ON s.ClassID = c.ClassID " +
                               "WHERE c.ClassName = @ClassName AND c.Discipline = @Discipline AND s.SubjectName = @SubjectName";

                using (SqlCommand command = new SqlCommand(query, connection, transaction))
                {
                    command.Parameters.AddWithValue("@ClassName", className);
                    command.Parameters.AddWithValue("@Discipline", discipline);
                    command.Parameters.AddWithValue("@SubjectName", subjectName);

                    // Execute the command and get the result
                    object result = command.ExecuteScalar();

                    // Check if the result is not null and convert it to an integer
                    if (result != null && result != DBNull.Value)
                    {
                        subjectID = Convert.ToInt32(result);
                    }
                }

                return subjectID;
            }

            public static (string, string) ValidateCredentials(string email, string password)
            {
                string id = null; // Default value indicating failure
                string role = null;

                // Check if the email exists in the Persons table
                string personIDQuery = "SELECT ID FROM Person WHERE Email = @Email";

                using (var connection = Configuration.getInstance().getConnection())
                {

                    try
                    {
                        using (SqlCommand command = new SqlCommand(personIDQuery, connection))
                        {
                            command.Parameters.AddWithValue("@Email", email);

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    int personIDValue = reader.GetInt32(0);

                                    // Close the first SqlDataReader
                                    reader.Close();

                                    // Email exists in Persons table, now check authentication
                                    string authenticationQuery = "SELECT ID FROM Authentication WHERE ID = @PersonID AND Password = @Password";
                                    Console.WriteLine("Test 1");

                                    using (SqlCommand authCommand = new SqlCommand(authenticationQuery, connection))
                                    {
                                        authCommand.Parameters.AddWithValue("@PersonID", personIDValue);
                                        authCommand.Parameters.AddWithValue("@Password", password);

                                        using (SqlDataReader authReader = authCommand.ExecuteReader())
                                        {
                                            if (authReader.Read())
                                            {
                                                // Email and password match in Authentication table
                                                // Now determine if the person is an Admin or Employee
                                                string adminQuery = "SELECT AdminID FROM Admin WHERE ID = @PersonID";
                                                string employeeQuery = "SELECT EmployeeID FROM Employee WHERE ID = @PersonID";
                                                authReader.Close();
                                                Console.WriteLine("Test 2");

                                                using (SqlCommand adminCommand = new SqlCommand(adminQuery, connection))
                                                {
                                                    adminCommand.Parameters.AddWithValue("@PersonID", personIDValue);

                                                    using (SqlDataReader adminReader = adminCommand.ExecuteReader())
                                                    {
                                                        if (adminReader.Read())
                                                        {
                                                            // Person is an Admin
                                                            id = adminReader.GetValue(0).ToString();
                                                            role = "Admin";
                                                            Console.WriteLine("Test 3");
                                                        }
                                                        else
                                                        {
                                                            // Person is an Employee
                                                            adminReader.Close();
                                                            using (SqlCommand employeeCommand = new SqlCommand(employeeQuery, connection))
                                                            {
                                                                employeeCommand.Parameters.AddWithValue("@PersonID", personIDValue);

                                                                using (SqlDataReader employeeReader = employeeCommand.ExecuteReader())
                                                                {
                                                                    if (employeeReader.Read())
                                                                    {
                                                                        id = employeeReader.GetValue(0).ToString();
                                                                        role = "Employee";
                                                                        employeeReader.Close();
                                                                        Console.WriteLine("Test 4");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    finally
                    {
                        // Close the connection in the finally block
                        connection.Close();
                    }
                }
                Console.WriteLine(id);
                Console.WriteLine(role);
                return (id, role); // Return the AdminID or EmployeeID
            }



            // Helper method to validate email format
            public static bool IsValidEmail(string email)
            {
                try
                {
                    var addr = new System.Net.Mail.MailAddress(email);
                    return addr.Address == email;
                }
                catch
                {
                    return false;
                }
            }

            public static bool CheckEmailExists(string email)
            {
                bool emailExists = false;
                string query = "SELECT COUNT(*) FROM Persons WHERE Email = @Email";

                using (SqlConnection connection = Configuration.getInstance().getConnection())
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Email", email);

                        int count = Convert.ToInt32(command.ExecuteScalar());
                        emailExists = (count > 0);
                    }
                }

                return emailExists;
            }

        }
    }


