﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ACDMS.Supporting_Classes

{
    class Configuration
    {
        String ConnectionStr = @"Data Source=(local);Initial Catalog=ACDMS;Integrated Security=True";
        SqlConnection con;
        private static Configuration _instance;
        public static Configuration getInstance()
        {
            if (_instance == null)
                _instance = new Configuration();
            return _instance;
        }
        private Configuration()
        {
            con = new SqlConnection(ConnectionStr);
            con.Open();
        }
        public SqlConnection getConnection()
        {
            if (con.State != ConnectionState.Open)
            {
                con.ConnectionString = ConnectionStr; // Set the connection string
                con.Open();
            }
            return con;
        }

    }
}