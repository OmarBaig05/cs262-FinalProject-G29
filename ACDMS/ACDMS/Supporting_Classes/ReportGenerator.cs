﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ACDMS.Supporting_Classes
{
    public class ReportGenerator
    {
        private Configuration connection;

        public ReportGenerator()
        {
            connection = Configuration.getInstance();
        }

        public DataTable GenerateClassWiseAttendanceReport(DateTime startDate, DateTime endDate)
        {
            DataTable dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand("GenerateClassWiseAttendanceReport", connection.getConnection()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StartDate", startDate);
                cmd.Parameters.AddWithValue("@EndDate", endDate);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;

        }
        public DataTable GenerateTodayAttendance()
        {
            DataTable dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand("GetTodaysAttendanceReport", connection.getConnection()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
        

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;

        }
        public DataTable GeneratePNL()
        {
            DataTable dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand("profitandloss", connection.getConnection()))
            {
                cmd.CommandType = CommandType.StoredProcedure;


                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;

        }

        public DataTable studentresults()
        {
            DataTable dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand("studentgrades", connection.getConnection()))
            {
                cmd.CommandType = CommandType.StoredProcedure;


                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;

        }
    }
}
