﻿using ACDMS.BL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACDMS.Supporting_Classes;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Xml.Linq;
using Microsoft.PowerBI.Api;

namespace ACDMS.DL
{
    internal class StudentDL
    {

        // Insertion
        public static bool InsertStudentData(StudentBL student)
        {
            // Establish database connection
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    if (!ValidateInput(student))
                    {
                        Console.WriteLine("StudentDL test 3");
                        return false;
                    }

                    // Insert data into Person table
                    int personID = InsertPersonData(connection, transaction, student);
                    Console.WriteLine("StudentDL test 1");

                    // Insert data into Student table
                    InsertStudentTable(connection, transaction, student.StudentID, personID);
                    Console.WriteLine("StudentDL test 2");
                    // Commit the transaction if all inserts are successful
                    transaction.Commit();
                    MessageBox.Show("Student data inserted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    MessageBox.Show("An error occurred: " + ex.Message);
                    Console.WriteLine("An error occurred: " + ex.Message);
                    Console.WriteLine("StudentDL test 4");
                    transaction.Rollback();
                    return false;
                }
            }
        }
        private static int InsertPersonData(SqlConnection connection, SqlTransaction transaction, StudentBL student)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "INSERT INTO Person (Name, FatherName, Email, ContactNumber, CNIC, Status, Gender, JoiningDate) " +
                                      "VALUES (@Name, @FatherName, @Email, @ContactNumber, @CNIC, @Status, @Gender, @JoiningDate);" +
                                      "SELECT SCOPE_IDENTITY();";
                command.Parameters.AddWithValue("@Name", student.Name);
                command.Parameters.AddWithValue("@FatherName", student.FatherName);
                command.Parameters.AddWithValue("@Email", student.Email);
                command.Parameters.AddWithValue("@ContactNumber", student.ContactNumber);
                command.Parameters.AddWithValue("@CNIC", student.CNIC);
                command.Parameters.AddWithValue("@Status", student.Status);
                command.Parameters.AddWithValue("@Gender", student.Gender);
                command.Parameters.AddWithValue("@JoiningDate", student.JoiningDate);

                // Execute the command and return the newly generated ID
                Console.WriteLine("Insert person");
                return Convert.ToInt32(command.ExecuteScalar());
            }
        }

        private static void InsertStudentTable(SqlConnection connection, SqlTransaction transaction, string studentID, int personID)
        {
            string insertStudentQuery = "INSERT INTO [dbo].[Student] (StudentID, ID) VALUES (@StudentID, @PersonID)";

            using (SqlCommand command = new SqlCommand(insertStudentQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@StudentID", studentID);
                command.Parameters.AddWithValue("@PersonID", personID);

                command.ExecuteNonQuery();
            }
        }

        // Update
        public static bool UpdateStudentData(StudentBL student,int empID, string message = "Student data updated")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Check if the studentID exists in the Student table
                    if (!IsStudentIDExists(connection, transaction, student.StudentID))
                    {
                        MessageBox.Show("StudentID does not exist.");
                        return false;
                    }
                    if (!ValidateInput(student))
                    {
                        return false;
                    }

                    UpdateStudent(connection, transaction, student);

                    // Log the changes to History table
                    int i = GetPersonIDByStudentID(student.StudentID);
                    History.LogChanges(connection, transaction, empID, message, "Student",i);

                    // Commit the transaction if all updates are successful
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void UpdateStudent(SqlConnection connection, SqlTransaction transaction, StudentBL student)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "UPDATE Student SET Name = @Name, Email = @Email, ContactNumber = @ContactNumber, " +
                                      "CNIC = @CNIC, FatherName = @FatherName, Gender = @Gender, JoiningDate = @JoiningDate, Status = @Status " +
                                      "WHERE StudentID = @StudentID";
                command.Parameters.AddWithValue("@Name", student.Name);
                command.Parameters.AddWithValue("@Email", student.Email);
                command.Parameters.AddWithValue("@ContactNumber", student.ContactNumber);
                command.Parameters.AddWithValue("@CNIC", student.CNIC);
                command.Parameters.AddWithValue("@FatherName", student.FatherName);
                command.Parameters.AddWithValue("@Gender", student.Gender);
                command.Parameters.AddWithValue("@JoiningDate", student.JoiningDate);
                command.Parameters.AddWithValue("@StudentID", student.StudentID);
                command.Parameters.AddWithValue("@Status", student.Status);
                command.ExecuteNonQuery();
            }
        }

        // Deletion
        public static bool DeleteStudent(string studentID,int empID, string message = "Status changed to InActive")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Update Student status to InActive

                    int v = Functions.GetLookUpValue("Inactive");

                    UpdateStudentStatus(connection, transaction, studentID,v);

                    // Log the change to History table
                    int i = GetPersonIDByStudentID(studentID);
                    History.LogChanges(connection, transaction, empID, message, "Student",i);

                    // Commit the transaction if all updates are successful
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void UpdateStudentStatus(SqlConnection connection, SqlTransaction transaction, string studentID, int LookUpID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "UPDATE Student SET Status = LookUpID WHERE StudentID = @StudentID";
                command.Parameters.AddWithValue("@StudentID", studentID);
                command.ExecuteNonQuery();
            }
        }

        // Helping Functions
        private static bool IsStudentIDExists(SqlConnection connection, SqlTransaction transaction, string studentID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "SELECT COUNT(*) FROM Student WHERE StudentID = @StudentID";
                command.Parameters.AddWithValue("@StudentID", studentID);
                int count = Convert.ToInt32(command.ExecuteScalar());
                return count > 0;
            }
        }


        public static bool ValidateInput(StudentBL student)
        {
            // Validate first name
            if (string.IsNullOrEmpty(student.Name))
            {
                MessageBox.Show("First name cannot be empty");
                return false;
            }

            if (!Regex.IsMatch(student.CNIC, @"^\d{5}-\d{7}-\d{1}$"))
            {
                MessageBox.Show("CNIC is not in a valid format.");
                return false;
            }
            // Validate FatherName
            if (string.IsNullOrEmpty(student.FatherName))
            {
                MessageBox.Show("Father name cannot be empty");
                return false;
            }

            // Validate contact
            if (!Regex.IsMatch(student.ContactNumber, @"^\d{11}$"))
            {
                MessageBox.Show("Contact cannot be empty, formate should be (03340110322)");
                return false;
            }

            // Validate email
            if (string.IsNullOrEmpty(student.Email))
            {
                MessageBox.Show("Email cannot be empty");
                return false;
            }
            else if (!Functions.IsValidEmail(student.Email))
            {
                MessageBox.Show("Invalid email format");
                return false;
            }

            // Validate registration number
            if (string.IsNullOrEmpty(student.StudentID))
            {
                MessageBox.Show("Student ID cannot be empty");
                return false;
            }
            else if(!IsValidStudentID(student.StudentID))
            {
                MessageBox.Show("Student ID format is invalid");
                return false;
            }

            return true;
        }
        public static bool IsValidStudentID(string studentID)
        {
            // Define the regex pattern for the student ID format
            string pattern = @"^\d{4}-(0[1-9]|1[0-2])-\d{3}$";

            // Check if the provided student ID matches the pattern
            return Regex.IsMatch(studentID, pattern);
        }
        public static int GetPersonIDByStudentID(string studentID)
        {
            int personID = -1; // Initialize with a default value indicating failure

            // SQL query to retrieve PersonID from Admin table
            string query = "SELECT ID FROM Student WHERE StudentID = @StudentID";

            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Add parameter to the command
                    command.Parameters.AddWithValue("@StudentID", studentID);

                    // Execute the command and get the result
                    object result = command.ExecuteScalar();

                    // Check if the result is not null and convert it to an integer
                    if (result != null && result != DBNull.Value)
                    {
                        personID = Convert.ToInt32(result);
                    }
                }
            }

            return personID;
        }
    }
}
