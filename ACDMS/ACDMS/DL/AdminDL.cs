﻿using ACDMS.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Windows.Forms;
using System.Data.SqlClient;
using ACDMS.Supporting_Classes;

namespace ACDMS.DL
{
    internal class AdminDL
    {

        //Insertion
        public static bool InsertAdminData(AdminBL admin)
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    if (IsAdminIDExists(connection, transaction, admin.AdminID))
                    {
                        MessageBox.Show("AdminID exists.");
                        return false;
                    }
                    if (!ValidateAdminData(admin))
                    {
                        return false;
                    }
                    // Insert data into the Person table and retrieve the newly generated ID
                    int personID = InsertPersonData(connection, transaction, admin);

                    // Insert data into the Authentication table
                    InsertAuthenticationData(connection, transaction, personID, admin.Password);

                    // Insert data into the Admin table
                    InsertAdminTable(connection, transaction, personID, admin.AdminID);

                    // Commit the transaction if all inserts are successful
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static int InsertPersonData(SqlConnection connection, SqlTransaction transaction, AdminBL admin)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "INSERT INTO Person (Name, FatherName, Email, ContactNumber, CNIC, Status, Gender, JoiningDate) " +
                                      "VALUES (@Name, @FatherName, @Email, @ContactNumber, @CNIC, @Status, @Gender, @JoiningDate);" +
                                      "SELECT SCOPE_IDENTITY();";
                command.Parameters.AddWithValue("@Name", admin.Name);
                command.Parameters.AddWithValue("@FatherName", admin.FatherName);
                command.Parameters.AddWithValue("@Email", admin.Email);
                command.Parameters.AddWithValue("@ContactNumber", admin.ContactNumber);
                command.Parameters.AddWithValue("@CNIC", admin.CNIC);
                command.Parameters.AddWithValue("@Status", admin.Status);
                command.Parameters.AddWithValue("@Gender", admin.Gender);
                command.Parameters.AddWithValue("@JoiningDate", admin.JoiningDate);

                // Execute the command and return the newly generated ID
                return Convert.ToInt32(command.ExecuteScalar());
            }
        }

        private static void InsertAuthenticationData(SqlConnection connection, SqlTransaction transaction, int personID, string password)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "INSERT INTO Authentication (ID, Password) VALUES (@ID, @Password)";
                command.Parameters.AddWithValue("@ID", personID);
                command.Parameters.AddWithValue("@Password", password);
                command.ExecuteNonQuery();
            }
        }

        private static void InsertAdminTable(SqlConnection connection, SqlTransaction transaction, int personID, string adminID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "INSERT INTO Admin (AdminID, ID) VALUES (@AdminID, @ID)";
                command.Parameters.AddWithValue("@AdminID", adminID);
                command.Parameters.AddWithValue("@ID", personID);
                command.ExecuteNonQuery();
            }
        }
        // Deletion
        public static bool DeleteAdmin(string adminID, int adm, string message = "Status changed to InActive")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Update Admin status to InActive
                    int v = Functions.GetLookUpValue("Inactive");
                    UpdateAdminStatus(connection, transaction, adminID, v);

                    // Log the change to AdminHistory table
                    int i = GetPersonIDByAdminID(adminID);
                    History.LogChanges(connection, transaction, adm, message, "Admin", i);

                    // Commit the transaction if all updates are successful
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void UpdateAdminStatus(SqlConnection connection, SqlTransaction transaction, string adminID, int LookUpID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "UPDATE Admin SET Status = LookUpID WHERE AdminID = @AdminID";
                command.Parameters.AddWithValue("@AdminID", adminID);
                command.ExecuteNonQuery();
            }
        }

        // Update
        public static bool UpdateAdminData(AdminBL admin, int adm, string message = "Admin data updated")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Check if the adminID exists in the Admin table
                    if (!IsAdminIDExists(connection, transaction, admin.AdminID))
                    {
                        MessageBox.Show("AdminID does not exist.");
                        return false;
                    }
                    if (!ValidateAdminData(admin))
                    {
                        return false ;
                    }


                    // Update admin data
                    UpdateAdmin(connection, transaction, admin);

                    // Log the changes to AdminHistory table
                    int i = GetPersonIDByAdminID(admin.AdminID);
                    History.LogChanges(connection, transaction, adm, message, "Admin", i);

                    // Commit the transaction if all updates are successful
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        private static void UpdateAdmin(SqlConnection connection, SqlTransaction transaction, AdminBL admin)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "UPDATE Admin SET Name = @Name, Email = @Email, ContactNumber = @ContactNumber, " +
                                      "CNIC = @CNIC, FatherName = @FatherName, Gender = @Gender, JoiningDate = @JoiningDate, Status = @Status " +
                                      "WHERE AdminID = @AdminID";
                command.Parameters.AddWithValue("@Name", admin.Name);
                command.Parameters.AddWithValue("@Email", admin.Email);
                command.Parameters.AddWithValue("@ContactNumber", admin.ContactNumber);
                command.Parameters.AddWithValue("@CNIC", admin.CNIC);
                command.Parameters.AddWithValue("@FatherName", admin.FatherName);
                command.Parameters.AddWithValue("@Gender", admin.Gender);
                command.Parameters.AddWithValue("@JoiningDate", admin.JoiningDate);
                command.Parameters.AddWithValue("@AdminID", admin.AdminID);
                command.Parameters.AddWithValue("@Status", admin.Status);
                command.ExecuteNonQuery();
            }
        }

        // Helping Functions
        private static bool IsAdminIDExists(SqlConnection connection, SqlTransaction transaction, string adminID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "SELECT COUNT(*) FROM Admin WHERE AdminID = @AdminID";
                command.Parameters.AddWithValue("@AdminID", adminID);
                int count = Convert.ToInt32(command.ExecuteScalar());
                return count > 0;
            }
        }

        public static bool ValidateAdminData(AdminBL admin)
        {
            //// Check if AdminID follows the format (you can define your specific format here)
            //if (!Regex.IsMatch(admin.AdminID, @"^[A-Za-z0-9]{4}$"))
            //{
            //    MessageBox.Show("AdminID is not in the correct format.");
            //    return false;
            //}

            // Check if Email is in a valid format
            if (!Functions.IsValidEmail(admin.Email))
            {
                MessageBox.Show("Email is not in a valid format.");
                return false;
            }

            // Check if ContactNumber is in a valid format (assuming 11 digits)
            if (!Regex.IsMatch(admin.ContactNumber, @"^\d{11}$"))
            {
                MessageBox.Show("ContactNumber is not in a valid format, formate should be (03340110322)");
                return false;
            }

            // Check if CNIC is in a valid format (assuming 15 characters with hyphens)
            if (!Regex.IsMatch(admin.CNIC, @"^\d{5}-\d{7}-\d{1}$"))
            {
                MessageBox.Show("CNIC is not in a valid format.");
                return false;
            }

            // Check if JoiningDate is not in the future
            if (admin.JoiningDate > DateTime.Now)
            {
                MessageBox.Show("JoiningDate cannot be in the future.");
                return false;
            }
            return true;
        }

        public static int GetPersonIDByAdminID(string adminID)
        {
            int personID = -1; // Initialize with a default value indicating failure

            // SQL query to retrieve PersonID from Admin table
            string query = "SELECT ID FROM Admin WHERE AdminID = @AdminID";

            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Add parameter to the command
                    command.Parameters.AddWithValue("@AdminID", adminID);

                    // Execute the command and get the result
                    object result = command.ExecuteScalar();

                    // Check if the result is not null and convert it to an integer
                    if (result != null && result != DBNull.Value)
                    {
                        personID = Convert.ToInt32(result);
                    }
                }
            }

            return personID;
        }

    }
}
