﻿using ACDMS.BL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.DL
{
    internal class EmployeeDL
    {

        //Insert function
        public static bool InsertEmployeeData(EmployeeBL employee)
        {
            // Establish database connection
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    if (!ValidateInput(employee))
                    {
                        return false;
                    }

                    // Check if the role ID exists in the Role table
                    if (!IsRoleIDExists(connection, transaction, employee.RoleID))
                    {
                        MessageBox.Show("RoleID does not exist.");
                        return false;
                    }
                    // Insert data into Person table
                    int personID = InsertPersonData(connection, transaction, employee);

                    // Insert data into Employee table
                    InsertEmployeeTable(connection, transaction, employee.EmployeeID, personID, employee.EmployeeSalary, employee.RoleID);

                    // Commit the transaction if all inserts are successful
                    transaction.Commit();
                    MessageBox.Show("Employee data inserted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    MessageBox.Show("An error occurred: " + ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void InsertEmployeeTable(SqlConnection connection, SqlTransaction transaction, string employeeID, int personID, decimal employeeSalary, int roleID)
        {
            string insertEmployeeQuery = "INSERT INTO [dbo].[Employee] (EmployeeID, ID, EmployeeSalary, RoleID) VALUES (@EmployeeID, @PersonID, @EmployeeSalary, @RoleID)";

            using (SqlCommand command = new SqlCommand(insertEmployeeQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@EmployeeID", employeeID);
                command.Parameters.AddWithValue("@PersonID", personID);
                command.Parameters.AddWithValue("@EmployeeSalary", employeeSalary);
                command.Parameters.AddWithValue("@RoleID", roleID);

                command.ExecuteNonQuery();
            }
        }

        private static int InsertPersonData(SqlConnection connection, SqlTransaction transaction, EmployeeBL employee)
        {
            string insertPersonQuery = "INSERT INTO [dbo].[Person] (Name, FatherName, Email, ContactNumber, CNIC, Status, Gender, JoiningDate) " +
                                       "VALUES (@Name, @FatherName, @Email, @ContactNumber, @CNIC, @Status, @Gender, @JoiningDate); " +
                                       "SELECT SCOPE_IDENTITY();";

            using (SqlCommand command = new SqlCommand(insertPersonQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@Name", employee.Name);
                command.Parameters.AddWithValue("@FatherName", employee.FatherName);
                command.Parameters.AddWithValue("@Email", employee.Email);
                command.Parameters.AddWithValue("@ContactNumber", employee.ContactNumber);
                command.Parameters.AddWithValue("@CNIC", employee.CNIC);
                command.Parameters.AddWithValue("@Status", employee.Status);
                command.Parameters.AddWithValue("@Gender", employee.Gender);
                command.Parameters.AddWithValue("@JoiningDate", employee.JoiningDate);

                return Convert.ToInt32(command.ExecuteScalar());
            }
        }

        // Update
        public static bool UpdateEmployeeData(EmployeeBL employee, int empID, string message = "Employee data updated")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Check if the employeeID exists in the Employee table
                    if (!IsEmployeeIDExists(connection, transaction, employee.EmployeeID))
                    {
                        MessageBox.Show("EmployeeID does not exist.");
                        return false;
                    }
                    if (!ValidateInput(employee))
                    {
                        return false;
                    }

                    // Update employee data
                    UpdateEmployee(connection, transaction, employee);

                    // Log the changes to History table
                    int personID = GetPersonIDByEmployeeID(employee.EmployeeID);
                    History.LogChanges(connection, transaction, empID, message, "Employee", personID);

                    // Commit the transaction if all updates are successful
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void UpdateEmployee(SqlConnection connection, SqlTransaction transaction, EmployeeBL employee)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "UPDATE Employee SET Name = @Name, Email = @Email, ContactNumber = @ContactNumber, " +
                                      "CNIC = @CNIC, FatherName = @FatherName, Gender = @Gender, JoiningDate = @JoiningDate, Status = @Status, EmployeeSalary = @EmployeeSalary " +
                                      "WHERE EmployeeID = @EmployeeID";
                command.Parameters.AddWithValue("@Name", employee.Name);
                command.Parameters.AddWithValue("@Email", employee.Email);
                command.Parameters.AddWithValue("@ContactNumber", employee.ContactNumber);
                command.Parameters.AddWithValue("@CNIC", employee.CNIC);
                command.Parameters.AddWithValue("@FatherName", employee.FatherName);
                command.Parameters.AddWithValue("@Gender", employee.Gender);
                command.Parameters.AddWithValue("@JoiningDate", employee.JoiningDate);
                command.Parameters.AddWithValue("@EmployeeID", employee.EmployeeID);
                command.Parameters.AddWithValue("@Status", employee.Status);
                command.Parameters.AddWithValue("@EmployeeSalary", employee.EmployeeSalary);
                command.ExecuteNonQuery();
            }
        }

        // Deletion
        public static bool DeleteEmployee(string employeeID, int empID, string message = "Status changed to InActive")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Update Employee status to InActive
                    int v = Functions.GetLookUpValue("Inactive");
                    UpdateEmployeeStatus(connection, transaction, employeeID, v);

                    // Log the change to History table
                    int personID = GetPersonIDByEmployeeID(employeeID);
                    History.LogChanges(connection, transaction, empID, message, "Employee", personID);

                    // Commit the transaction if all updates are successful
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void UpdateEmployeeStatus(SqlConnection connection, SqlTransaction transaction, string employeeID, int LookUpID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "UPDATE Employee SET Status = LookUpID WHERE EmployeeID = @EmployeeID";
                command.Parameters.AddWithValue("@EmployeeID", employeeID);
                command.ExecuteNonQuery();
            }
        }


        // Helping Functions
        public static int GetPersonIDByEmployeeID(string employeeID)
        {
            int personID = -1; // Initialize with a default value indicating failure

            // SQL query to retrieve PersonID from Employee table
            string query = "SELECT ID FROM Employee WHERE EmployeeID = @EmployeeID";

            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Add parameter to the command
                    command.Parameters.AddWithValue("@EmployeeID", employeeID);

                    // Execute the command and get the result
                    object result = command.ExecuteScalar();

                    // Check if the result is not null and convert it to an integer
                    if (result != null && result != DBNull.Value)
                    {
                        personID = Convert.ToInt32(result);
                    }
                }
            }

            return personID;
        }
        private static bool IsRoleIDExists(SqlConnection connection, SqlTransaction transaction, int roleID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "SELECT COUNT(*) FROM Role WHERE RoleID = @RoleID";
                command.Parameters.AddWithValue("@RoleID", roleID);
                int count = Convert.ToInt32(command.ExecuteScalar());
                return count > 0;
            }
        }
        public static bool ValidateInput(EmployeeBL employee)
        {
            // Validate employee name
            if (string.IsNullOrEmpty(employee.Name))
            {
                MessageBox.Show("Employee name cannot be empty");
                return false;
            }

            // Validate CNIC
            if (!Regex.IsMatch(employee.CNIC, @"^\d{5}-\d{7}-\d{1}$"))
            {
                MessageBox.Show("CNIC is not in a valid format.");
                return false;
            }

            // Validate father's name
            if (string.IsNullOrEmpty(employee.FatherName))
            {
                MessageBox.Show("Father's name cannot be empty");
                return false;
            }

            // Validate contact number
            if (!Regex.IsMatch(employee.ContactNumber, @"^\d{11}$"))
            {
                MessageBox.Show("Contact number is not in a valid format, format should be (03340110322)");
                return false;
            }

            // Validate email
            if (string.IsNullOrEmpty(employee.Email))
            {
                MessageBox.Show("Email cannot be empty");
                return false;
            }
            else if (!Functions.IsValidEmail(employee.Email))
            {
                MessageBox.Show("Invalid email format");
                return false;
            }

            // Validate employee ID
            if (string.IsNullOrEmpty(employee.EmployeeID))
            {
                MessageBox.Show("Employee ID cannot be empty");
                return false;
            }

            // Validate joining date
            if (employee.JoiningDate > DateTime.Now)
            {
                MessageBox.Show("Joining date cannot be in the future");
                return false;
            }

            return true;
        }
        private static bool IsEmployeeIDExists(SqlConnection connection, SqlTransaction transaction, string employeeID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "SELECT COUNT(*) FROM Employee WHERE EmployeeID = @EmployeeID";
                command.Parameters.AddWithValue("@EmployeeID", employeeID);
                int count = Convert.ToInt32(command.ExecuteScalar());
                return count > 0;
            }
        }
    }
}
