﻿using ACDMS.BL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACDMS.DL
{
    internal class CoursesDL
    {
        // Insert Class
        public static bool InsertClassData(CoursesBL course)
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Validate input data
                    if (!ValidateInput(course))
                    {
                        MessageBox.Show("Input data is not valid.");
                        return false;
                    }

                    // Insert data into Class table
                    int classID = InsertClass(connection, transaction, course);

                    // Insert data into Subject table
                    InsertSubject(connection, transaction, course.Subject, classID, course.Status, course.Discipline);

                    // Commit the transaction if all inserts are successful
                    transaction.Commit();
                    MessageBox.Show("Class and Subject data inserted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    MessageBox.Show("An error occurred: " + ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static int InsertClass(SqlConnection connection, SqlTransaction transaction, CoursesBL course)
        {
            string insertClassQuery = "INSERT INTO Class (ClassName, Status, Discipline) " +
                                      "VALUES (@ClassName, @Status, @Discipline); " +
                                      "SELECT SCOPE_IDENTITY();";

            using (SqlCommand command = new SqlCommand(insertClassQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@ClassName", course.Class);
                command.Parameters.AddWithValue("@Status", course.Status);
                command.Parameters.AddWithValue("@Discipline", course.Discipline);

                return Convert.ToInt32(command.ExecuteScalar());
            }
        }

        private static void InsertSubject(SqlConnection connection, SqlTransaction transaction, string subject, int classID, int status, string discipline)
        {
            string insertSubjectQuery = "INSERT INTO Subject (SubjectName, ClassID, Status, Discipline) " +
                                         "VALUES (@SubjectName, @ClassID, @Status, @Discipline);";

            using (SqlCommand command = new SqlCommand(insertSubjectQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@SubjectName", subject);
                command.Parameters.AddWithValue("@ClassID", classID);
                command.Parameters.AddWithValue("@Status", status);
                command.Parameters.AddWithValue("@Discipline", discipline);

                command.ExecuteNonQuery();
            }
        }

        // Deletion
        public static bool DeleteClassAndSubject(string className, string subjectName, int empID, string message = "Status changed to 0")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Update Class status to 0
                    UpdateClassStatus(connection, transaction, className);

                    // Log the change to History table for Class
                    int c = GetClassIDByClassName(className);
                    History.LogChanges(connection, transaction, empID, message, "Class", c);

                    // Update Subject status to 0
                    UpdateSubjectStatus(connection, transaction, subjectName);

                    // Log the change to History table for Subject
                    int s = GetSubjectIDBySubjectName(subjectName);
                    History.LogChanges(connection, transaction, empID, message, "Subject", s);

                    // Commit the transaction if all updates are successful
                    transaction.Commit();
                    MessageBox.Show("Class and Subject deleted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    MessageBox.Show("An error occurred: " + ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void UpdateClassStatus(SqlConnection connection, SqlTransaction transaction, string className)
        {
            string updateClassQuery = "UPDATE Class SET Status = 0 WHERE ClassName = @ClassName";

            using (SqlCommand command = new SqlCommand(updateClassQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@ClassName", className);
                command.ExecuteNonQuery();
            }
        }

        private static void UpdateSubjectStatus(SqlConnection connection, SqlTransaction transaction, string subjectName)
        {
            string updateSubjectQuery = "UPDATE Subject SET Status = 0 WHERE SubjectName = @SubjectName";

            using (SqlCommand command = new SqlCommand(updateSubjectQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@SubjectName", subjectName);
                command.ExecuteNonQuery();
            }
        }

        // Update
        public static bool UpdateClassAndSubject(string oldClassName, string oldSubjectName, CoursesBL newCourse, int empID, string message = "Class and Subject data updated")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Update Class data
                    UpdateClass(connection, transaction, oldClassName, newCourse.Class, newCourse.Status, newCourse.Discipline);

                    // Log the changes to History table for Class
                    int c = GetClassIDByClassName(oldClassName);
                    History.LogChanges(connection, transaction, empID, message, "Class", c);

                    // Update Subject data
                    UpdateSubject(connection, transaction, oldSubjectName, newCourse.Subject, newCourse.Status, newCourse.Discipline);

                    // Log the changes to History table for Subject
                    int s = GetSubjectIDBySubjectName(oldSubjectName);
                    History.LogChanges(connection, transaction, empID, message, "Subject", s);

                    // Commit the transaction if all updates are successful
                    transaction.Commit();
                    MessageBox.Show("Class and Subject data updated successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    MessageBox.Show("An error occurred: " + ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void UpdateClass(SqlConnection connection, SqlTransaction transaction, string oldClassName, string newClassName, int status, string discipline)
        {
            string updateClassQuery = "UPDATE Class SET ClassName = @NewClassName, Status = @Status, Discipline = @Discipline " +
                                      "WHERE ClassName = @OldClassName";

            using (SqlCommand command = new SqlCommand(updateClassQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@NewClassName", newClassName);
                command.Parameters.AddWithValue("@Status", status);
                command.Parameters.AddWithValue("@Discipline", discipline);
                command.Parameters.AddWithValue("@OldClassName", oldClassName);
                command.ExecuteNonQuery();
            }
        }

        private static void UpdateSubject(SqlConnection connection, SqlTransaction transaction, string oldSubjectName, string newSubjectName, int status, string discipline)
        {
            string updateSubjectQuery = "UPDATE Subject SET SubjectName = @NewSubjectName, Status = @Status, Discipline = @Discipline " +
                                        "WHERE SubjectName = @OldSubjectName";

            using (SqlCommand command = new SqlCommand(updateSubjectQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@NewSubjectName", newSubjectName);
                command.Parameters.AddWithValue("@Status", status);
                command.Parameters.AddWithValue("@Discipline", discipline);
                command.Parameters.AddWithValue("@OldSubjectName", oldSubjectName);
                command.ExecuteNonQuery();
            }
        }
        // Validate Input
        public static bool ValidateInput(CoursesBL course)
        {
            // Validate subject name
            if (string.IsNullOrEmpty(course.Subject))
            {
                MessageBox.Show("Subject name cannot be empty.");
                return false;
            }

            // Validate class name
            if (string.IsNullOrEmpty(course.Class))
            {
                MessageBox.Show("Class name cannot be empty.");
                return false;
            }

            // Validate discipline
            if (string.IsNullOrEmpty(course.Discipline))
            {
                MessageBox.Show("Discipline cannot be empty.");
                return false;
            }

            return true;
        }

        public static int GetSubjectIDBySubjectName(string subjectName)
        {
            int subjectID = -1; // Initialize with a default value indicating failure

            // SQL query to retrieve SubjectID from Subject table
            string query = "SELECT SubjectID FROM Subject WHERE SubjectName = @SubjectName";

            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Add parameter to the command
                    command.Parameters.AddWithValue("@SubjectName", subjectName);

                    // Execute the command and get the result
                    object result = command.ExecuteScalar();

                    // Check if the result is not null and convert it to an integer
                    if (result != null && result != DBNull.Value)
                    {
                        subjectID = Convert.ToInt32(result);
                    }
                }
            }

            return subjectID;
        }

        // Get ClassID by ClassName
        public static int GetClassIDByClassName(string className)
        {
            int classID = -1; // Initialize with a default value indicating failure

            // SQL query to retrieve ClassID from Class table
            string query = "SELECT ClassID FROM Class WHERE ClassName = @ClassName";

            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Add parameter to the command
                    command.Parameters.AddWithValue("@ClassName", className);

                    // Execute the command and get the result
                    object result = command.ExecuteScalar();

                    // Check if the result is not null and convert it to an integer
                    if (result != null && result != DBNull.Value)
                    {
                        classID = Convert.ToInt32(result);
                    }
                }
            }

            return classID;
        }

        public static (string,string) GetClassAndSubjectName(int subjectID)
        {
            // Connection string (replace with your actual connection string)
            string connectionString = "Data Source=YourServer;Initial Catalog=ACDMS;Integrated Security=True";

            // SQL query to retrieve class and subject name
            string query = "SELECT Class.ClassName, Subject.SubjectName " +
                           "FROM Class INNER JOIN Subject ON Class.ClassID = Subject.ClassID " +
                           "WHERE Subject.SubjectID = @SubjectID";

            // Create connection and command objects
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                // Add parameter for subjectID
                command.Parameters.AddWithValue("@SubjectID", subjectID);

                try
                {
                    // Open connection
                    connection.Open();

                    // Execute query
                    SqlDataReader reader = command.ExecuteReader();

                    // Check if any rows are returned
                    if (reader.HasRows)
                    {
                        // Read the first row
                        reader.Read();

                        // Retrieve class and subject name
                        string className = reader.GetString(0);
                        string subjectName = reader.GetString(1);

                        // Display the retrieved data
                        reader.Close();
                        return (className, subjectName);
                    }
                    else
                    {
                        Console.WriteLine("No data found for the given subject ID.");
                        reader.Close();
                        return (null, null);
                    }

                    // Close the reader
                }
                catch (Exception ex)
                {
                    // Handle exceptions
                    Console.WriteLine("An error occurred: " + ex.Message);
                    return (null, null);
                }
            }
        }
    }
}
