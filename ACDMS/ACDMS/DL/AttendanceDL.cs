﻿using ACDMS.BL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.DL
{
    internal class AttendanceDL
    {
        public static bool MarkAttendance(AttendanceBL attendance)
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Check if attendance is already marked for the current date
                    if (IsAttendanceMarkedForToday(connection, transaction, attendance.PersonID, attendance.AttendanceDate))
                    {
                        Console.WriteLine("Attendance already marked for today.");
                        return false; // Return without inserting data
                    }

                    // Insert data into the Attendance table
                    InsertAttendanceData(connection, transaction, attendance);

                    // Commit the transaction if the insert is successful
                    transaction.Commit();
                    Console.WriteLine("Attendance marked successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static bool IsAttendanceMarkedForToday(SqlConnection connection, SqlTransaction transaction, int personID, DateTime attendanceDate)
        {
            string query = "SELECT COUNT(*) FROM Attendance WHERE PersonID = @PersonID AND AttendanceDate = @AttendanceDate";

            using (SqlCommand command = new SqlCommand(query, connection, transaction))
            {
                command.Parameters.AddWithValue("@PersonID", personID);
                command.Parameters.AddWithValue("@AttendanceDate", attendanceDate);

                int count = Convert.ToInt32(command.ExecuteScalar());
                return count > 0;
            }
        }

        private static void InsertAttendanceData(SqlConnection connection, SqlTransaction transaction, AttendanceBL attendance)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "INSERT INTO Attendance (PersonID, AttendanceDate, AttendanceStatus, AttendanceType) " +
                                      "VALUES (@PersonID, @AttendanceDate, @AttendanceStatus, @AttendanceType)";
                command.Parameters.AddWithValue("@PersonID", attendance.PersonID);
                command.Parameters.AddWithValue("@AttendanceDate", attendance.AttendanceDate);
                command.Parameters.AddWithValue("@AttendanceStatus", attendance.AttendanceStatus);
                command.Parameters.AddWithValue("@AttendanceType", attendance.AttendanceType);
                command.ExecuteNonQuery();
            }
        }


        public static bool DeleteAttendance(int attendanceID, int empID, string message = "Attendance deleted")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Get the PersonID associated with the attendance record
                    int personID = GetPersonIDByAttendanceID(connection, transaction, attendanceID);

                    int recordID = GetRecordIDByPersonID(personID);

                    // Delete the attendance record
                    DeleteAttendanceRecord(connection, transaction, attendanceID);

                    // Log the deletion action to the History table
                    LogAttendanceDeletion(connection, transaction, empID, message, recordID);

                    // Commit the transaction if the deletion is successful
                    transaction.Commit();
                    Console.WriteLine("Attendance deleted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static int GetPersonIDByAttendanceID(SqlConnection connection, SqlTransaction transaction, int attendanceID)
        {
            int personID = -1; // Initialize with a default value indicating failure

            string query = "SELECT PersonID FROM Attendance WHERE AttendanceID = @AttendanceID";

            using (SqlCommand command = new SqlCommand(query, connection, transaction))
            {
                command.Parameters.AddWithValue("@AttendanceID", attendanceID);

                // Execute the command and get the result
                object result = command.ExecuteScalar();

                // Check if the result is not null and convert it to an integer
                if (result != null && result != DBNull.Value)
                {
                    personID = Convert.ToInt32(result);
                }
            }

            return personID;
        }

        private static void DeleteAttendanceRecord(SqlConnection connection, SqlTransaction transaction, int attendanceID)
        {
            string deleteQuery = "DELETE FROM Attendance WHERE AttendanceID = @AttendanceID";

            using (SqlCommand command = new SqlCommand(deleteQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@AttendanceID", attendanceID);
                command.ExecuteNonQuery();
            }
        }

        private static void LogAttendanceDeletion(SqlConnection connection, SqlTransaction transaction, int empID, string message, int recordID)
        {
            string insertQuery = "INSERT INTO History (ID, ChangedEntity, ChangeType, TupleID, ChangeTime) " +
                                 "VALUES (@AdminID, @ChangedEntity, @ChangeType, @TupleID, GETDATE())";

            using (SqlCommand command = new SqlCommand(insertQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@AdminID", empID);
                command.Parameters.AddWithValue("@ChangedEntity", "Attendance");
                command.Parameters.AddWithValue("@ChangeType", message);
                command.Parameters.AddWithValue("@TupleID", recordID);
                command.ExecuteNonQuery();
            }
        }

        public static int GetRecordIDByPersonID(int personID)
        {
            int recordID = -1; // Initialize with a default value indicating failure

            string query = "SELECT RecordID FROM History WHERE ID = @PersonID";

            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@PersonID", personID);

                    object result = command.ExecuteScalar();

                    if (result != null && result != DBNull.Value)
                    {
                        recordID = Convert.ToInt32(result);
                    }
                }
            }

            return recordID;
        }
    }
}
