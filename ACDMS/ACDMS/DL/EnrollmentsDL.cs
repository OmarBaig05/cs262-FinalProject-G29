﻿using ACDMS.BL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.DL
{
    internal class EnrollmentsDL
    {
        public static bool InsertEnrollment(EnrollmentBL enrollment)
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Insert data into the Enrollment table
                    InsertEnrollmentData(connection, transaction, enrollment);

                    // Commit the transaction if the insert is successful
                    transaction.Commit();
                    Console.WriteLine("Enrollment inserted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void InsertEnrollmentData(SqlConnection connection, SqlTransaction transaction, EnrollmentBL enrollment)
        {
            string insertQuery = "INSERT INTO Enrollment (PersonID, SubjectID, EnrollmentDate) " +
                                 "VALUES (@PersonID, @SubjectID, @EnrollmentDate)";

            using (SqlCommand command = new SqlCommand(insertQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@PersonID", enrollment.PersonID);
                command.Parameters.AddWithValue("@SubjectID", enrollment.SubjectID);
                command.Parameters.AddWithValue("@EnrollmentDate", enrollment.DateTime);
                command.ExecuteNonQuery();
            }
        }


        public static bool DeleteEnrollment(EnrollmentBL enrollment, int empID, string message = "Enrollment deleted")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Delete the enrollment record
                    DeleteEnrollmentRecord(connection, transaction, enrollment);

                    // Log the deletion action to the History table
                    LogEnrollmentDeletion(connection, transaction, empID, message);

                    // Commit the transaction if the deletion is successful
                    transaction.Commit();
                    Console.WriteLine("Enrollment deleted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;   
                }
            }
        }

        private static void DeleteEnrollmentRecord(SqlConnection connection, SqlTransaction transaction, EnrollmentBL enrollment)
        {
            string deleteQuery = "DELETE FROM Enrollment WHERE PersonID = @PersonID and SubjectID = @SubjectID";

            using (SqlCommand command = new SqlCommand(deleteQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@EnrollmentID", enrollment.PersonID);
                command.Parameters.AddWithValue("@EnrollmentID", enrollment.SubjectID);
                command.ExecuteNonQuery();
            }
        }

        private static void LogEnrollmentDeletion(SqlConnection connection, SqlTransaction transaction, int empID, string message)
        {
            string insertQuery = "INSERT INTO History (ID, ChangedEntity, ChangeType, TupleID, ChangeTime) " +
                                 "VALUES (@AdminID, @ChangedEntity, @ChangeType, @TupleID, GETDATE())";

            using (SqlCommand command = new SqlCommand(insertQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@AdminID", empID);
                command.Parameters.AddWithValue("@ChangedEntity", "Enrollment");
                command.Parameters.AddWithValue("@ChangeType", message);
                command.Parameters.AddWithValue("@TupleID", 0);
                command.ExecuteNonQuery();
            }
        }
    }
}
