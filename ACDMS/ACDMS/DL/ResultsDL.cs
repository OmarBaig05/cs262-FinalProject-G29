﻿using ACDMS.BL;
using ACDMS.Supporting_Classes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDMS.DL
{
    internal class ResultsDL
    {

        // Insertion
        public static bool InsertResult(ResultsBL result)
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Insert data into the Results table
                    InsertResultData(connection, transaction, result);

                    // Commit the transaction if the insert is successful
                    transaction.Commit();
                    Console.WriteLine("Result inserted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void InsertResultData(SqlConnection connection, SqlTransaction transaction, ResultsBL result)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "INSERT INTO Results (StudentID, TeacherID, SubjectID, ObtainedMarks, TotalMarks, ResultDate) " +
                                      "VALUES (@StudentID, @TeacherID, @SubjectID, @ObtainedMarks, @TotalMarks, @ResultDate)";
                command.Parameters.AddWithValue("@StudentID", result.StudentID);
                command.Parameters.AddWithValue("@TeacherID", result.TeacherID);
                command.Parameters.AddWithValue("@SubjectID", result.SubjectID);
                command.Parameters.AddWithValue("@ObtainedMarks", result.ObtainedMarks);
                command.Parameters.AddWithValue("@TotalMarks", result.TotalMarks);
                command.Parameters.AddWithValue("@ResultDate", result.ResultDate);
                command.ExecuteNonQuery();
            }
        }

        public static bool DeleteResult(int resultID, int empID, string message = "Result deleted")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Get the PersonID associated with the result record
                    int personID = GetPersonIDByResultID(connection, transaction, resultID);

                    // Delete the result record
                    DeleteResultRecord(connection, transaction, resultID);

                    // Log the deletion action to the History table
                    LogResultDeletion(connection, transaction, empID, message, resultID);

                    // Commit the transaction if the deletion is successful
                    transaction.Commit();
                    Console.WriteLine("Result deleted successfully.");
                    return true;
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private static void DeleteResultRecord(SqlConnection connection, SqlTransaction transaction, int resultID)
        {
            string deleteQuery = "DELETE FROM Results WHERE ResultID = @ResultID";

            using (SqlCommand command = new SqlCommand(deleteQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@ResultID", resultID);
                command.ExecuteNonQuery();
            }
        }


        public static void UpdateResult(int ResultID, ResultsBL result, int empID, string message = "Result updated")
        {
            using (SqlConnection connection = Configuration.getInstance().getConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Update the result record
                    UpdateResultData(connection, transaction, result,ResultID);

                    // Get the PersonID associated with the result record
                    int personID = GetPersonIDByResultID(connection, transaction, ResultID);

                    // Log the updation action to the History table
                    LogResultUpdation(connection, transaction, empID, message, ResultID);

                    // Commit the transaction if the updation is successful
                    transaction.Commit();
                    Console.WriteLine("Result updated successfully.");
                }
                catch (Exception ex)
                {
                    // Rollback the transaction if any error occurs
                    Console.WriteLine("Error occurred. Rolling back transaction.");
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
        }


        private static void UpdateResultData(SqlConnection connection, SqlTransaction transaction, ResultsBL result, int ResultID)
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandText = "UPDATE Results SET StudentID = @StudentID, TeacherID = @TeacherID, SubjectID = @SubjectID, " +
                                      "ObtainedMarks = @ObtainedMarks, TotalMarks = @TotalMarks, ResultDate = @ResultDate " +
                                      "WHERE ResultID = @ResultID";
                command.Parameters.AddWithValue("@StudentID", result.StudentID);
                command.Parameters.AddWithValue("@TeacherID", result.TeacherID);
                command.Parameters.AddWithValue("@SubjectID", result.SubjectID);
                command.Parameters.AddWithValue("@ObtainedMarks", result.ObtainedMarks);
                command.Parameters.AddWithValue("@TotalMarks", result.TotalMarks);
                command.Parameters.AddWithValue("@ResultDate", result.ResultDate);
                command.Parameters.AddWithValue("@ResultID", ResultID);
                command.ExecuteNonQuery();
            }
        }

        private static void LogResultDeletion(SqlConnection connection, SqlTransaction transaction, int empID, string message, int recordID)
        {
            string insertQuery = "INSERT INTO History (ID, ChangedEntity, ChangeType, TupleID, ChangeTime) " +
                                 "VALUES (@AdminID, @ChangedEntity, @ChangeType, @TupleID, GETDATE())";

            using (SqlCommand command = new SqlCommand(insertQuery, connection, transaction))
            {
                command.Parameters.AddWithValue("@AdminID", empID);
                command.Parameters.AddWithValue("@ChangedEntity", "Result");
                command.Parameters.AddWithValue("@ChangeType", message);
                command.Parameters.AddWithValue("@TupleID", recordID);
                command.ExecuteNonQuery();
            }
        }

        private static void LogResultUpdation(SqlConnection connection, SqlTransaction transaction, int empID, string message, int recordID)
        {
            LogResultDeletion(connection, transaction, empID, message, recordID);
        }

        private static int GetPersonIDByResultID(SqlConnection connection, SqlTransaction transaction, int resultID)
        {
            int personID = -1; // Initialize with a default value indicating failure

            string query = "SELECT StudentID FROM Results WHERE ResultID = @ResultID";

            using (SqlCommand command = new SqlCommand(query, connection, transaction))
            {
                command.Parameters.AddWithValue("@ResultID", resultID);

                // Execute the command and get the result
                object result = command.ExecuteScalar();

                // Check if the result is not null and convert it to an integer
                if (result != null && result != DBNull.Value)
                {
                    personID = Convert.ToInt32(result);
                }
            }

            return personID;
        }
    }
}
